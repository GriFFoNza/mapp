$(document).ready(function() {

    $('#addRuleTemplBtn').on('click', function () {
        var currules = $('#rulesTable tr');
        for (var i = 1; i < currules.length; ++i) {
           $('.rulematrixcell[data-level="' + currules[i].getAttribute('data-level-id') + '"][data-period="'+ currules[i].getAttribute('data-period-id') +'"]').addClass('bg-info');
        }
        $('#rulematrixModal').modal('show');
    });

    $('#rulematrix td').on('click', function () {
        if (!this.classList.contains('bg-info')) {
            this.classList.contains('bg-success') ? $(this).removeClass('bg-success').addClass('bg-light') : $(this).addClass('bg-success').removeClass('bg-light');
        }
    });

    $('#submitSaveListRuleBtn').on('click', function () {
        var submitcells = $('#rulematrix tr td.bg-success');
        var obj = [];
        for (var i = 0; i < submitcells.length; ++i) {
            var level = submitcells[i].getAttribute('data-level');
            var period = submitcells[i].getAttribute('data-period');
            var indid = $('#inputIndId').val();
            obj.push({'indid': indid,'ind_detal_level_id': level, 'ind_period_type_id' : period});
        }
        $.ajax({
            type: 'POST',
            url: '/plans/rules/savelist',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.ok  === 'Success') {
                    $('#rulematrixModal').modal('hide');
                     window.location.reload();
                }
            },
            error: function (result) {
                $('#errModalBody').text(result.responseJSON.error);
                $('#errModal').modal('show');
            }
        })
    });

    $('#rulematrix th.rulematrixcolumns').on('click', function () {
        var cells = $('.rulematrixcell:nth-child(' + ($(this).index() + 1) + ')');
        var Colhighlight = false;
        for (var i = 0; i < cells.length; ++i) {
            if (!(cells[i].classList.contains('bg-success') || cells[i].classList.contains('bg-info'))) {
                Colhighlight = true;
                break;
            }
        }
        if (Colhighlight) {
            for (var n = 0; n < cells.length; ++n) {
                if (!cells[n].classList.contains('bg-info')) {
                    cells[n].classList.add('bg-success');
                }
            }
         }
        else {
            cells.removeClass('bg-success');
        }
    });

    $('#rulematrix th.rulematrixrows').on('click', function () {
        var cells =  $(this).closest('tr').children('td');
        var Rowhighlight = false;
        for (var i = 0; i < cells.length; ++i) {
            if (!(cells[i].classList.contains('bg-success') || cells[i].classList.contains('bg-info'))) {
                Rowhighlight = true;
                break;
            }
        }
        if (Rowhighlight) {
            for (var n = 0; n < cells.length; ++n) {
                if (!cells[n].classList.contains('bg-info')) {
                    cells[n].classList.add('bg-success');
                }
            }
        }
        else {
            cells.removeClass('bg-success');
        }
    });

    $('.rulematrixcell').on('mouseover', function() {
        $(this).closest('tr').addClass('bg-light');
        var cells = $(this).closest('table').find('.rulematrixcell:nth-child(' + ($(this).index() + 1) + ')');
        for (var i = 0; i < cells.length; ++i) {
            if (!(cells[i].classList.contains('bg-success') || cells[i].classList.contains('bg-info'))) {
                cells[i].classList.add('bg-light');
            }
        }
    });

    $('.rulematrixcell').on('mouseout', function() {
        $(this).closest('tr').removeClass('bg-light');
        $(this).closest('table').find('.rulematrixcell:nth-child(' + ($(this).index() + 1) + ')').removeClass('bg-light');
    });


    $('table .deleteRule').on('click', function () {
        var id = $(this).parent().find('#RuleId').val();

        $('#deleteRuleId').val(id);
        $('#deleteRuleModalTitle').text('Удаление правила ' + id);

        $('#deleteRuleModal').modal('show');
    });

    $('#addRuleBtn').on('click', function () {

        $('#saveRuleModalTitle').text('Создание правила');
        $('#submitSaveRuleBtn').val('Добавить');

        $('#id').val('');
        $('#indid').val($('#inputIndId').val());

        $('#ind_period_type_id').prop('selectedIndex', 0);
        $('#ind_detal_level_id').prop('selectedIndex', 0);

        $('#saveRuleModal').modal('show');
    });

    $('table .editRule').on('click', function () {
        var id = $(this).parent().find('#RuleId').val();
        $.ajax({
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: '/plans/rules/find/' + id,
            success: function (rule) {
                $('#saveRuleModalTitle').text('Изменение правила');
                $('#submitSaveRuleBtn').val('Сохранить');

                $('#id').val(rule.id);
                $('#indid').val($('#inputIndId').val());

                $('#ind_period_type_id').val(rule.period.id);
                $('#ind_detal_level_id').val(rule.level.id);

                $('#saveRuleModal').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        })
    });

    $('#saveRuleForm').submit(function(e){
        e.preventDefault();
        var data = $('#saveRuleForm').serialize().split('&');
        var obj = {};
        for (var key in data) {
            obj[data[key].split('=')[0]] = data[key].split('=')[1];
        }
        $.ajax({
            type: 'POST',
            url: '/plans/rules/save',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.ok  === 'Success') {
                    $('#saveRuleModal').modal('hide');
                    window.location.reload();
                }
            },
            error: function (result) {
                $('#errModalBody').text(result.responseJSON.error);
                $('#errModal').modal('show');
            }
        })
    });

    $('#deleteRuleForm').submit(function(e){
        e.preventDefault();
        var data = $('#deleteRuleForm').serialize().split('&');
        var obj = {};
        for (var key in data) {
            obj[data[key].split('=')[0]] = data[key].split('=')[1];
        }
        $.ajax({
            type: 'DELETE',
            url: '/plans/rules/delete',
            data: JSON.stringify(obj),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                if (result.ok  === 'Success') {
                    $('#deleteRuleModal').modal('hide');
                    window.location.reload();
                }
            },
            error: function (result) {
                $('#errModalBody').text(result.responseJSON.error);
                $('#errModal').modal('show')
            }
        })
    });

    $('table .editRuleScale').on('click', function () {
        var id = $(this).parent().find('#RuleId').val();
        $.ajax({
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: '/plans/rules/findrulescales/' + id,
            success: function (rulescales) {

                $('#ind_rule_id').val(rulescales.id);

                $('#saveRuleScaleModal').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        })
    });

});