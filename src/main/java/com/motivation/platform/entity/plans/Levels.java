package com.motivation.platform.entity.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Data
@Entity
@Table(name = "plans_dic_ind_detal_levels", schema = "MAPP")
public class Levels {
//    --OK
//    id, number(38), ,
//    list_order_num, number(38), ,
//    list_short_value, varchar2(255), ,
//    list_full_value, varchar2(255), ,
//    unit_id, number,

    @NotBlank
    @Column(name = "list_short_value", nullable = false)
    public String list_short_value;
    @NotBlank
    @Column(name = "list_full_value", nullable = false)
    public String list_full_value;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @NotNull
    @Column(name = "list_order_num", nullable = false)
    private Long listordernum;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


