package com.motivation.platform.entity.plans;

import com.motivation.platform.config.validator.DateRangeValidator;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Entity
@DateRangeValidator(DateStart = "start_date", DateEnd = "finish_date")
@Table(name = "plans_dic_ind_responsible", schema = "MAPP")
public class Responsible {
    @NotBlank
    @Column(name = "ind_value_type", nullable = false)
    public String ind_value_type;
    @NotBlank
    @Column(name = "emp_name", nullable = false)
    public String emp_name;
    @NotBlank
    @Column(name = "calc_periodicity", nullable = false)
    public String calc_periodicity;
    @NotNull
    @Column(name = "start_date", nullable = false)
    public java.sql.Date start_date;
    @NotNull
    @Column(name = "finish_date", nullable = false)
    public java.sql.Date finish_date;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    //    --OK???
//    id, number(38), ,
//    indid, number(38), ,
//    ind_value_type, varchar2(4), ,
//    emp_id, number(38), ,
//    emp_name, varchar2(255), ,
//    calc_periodicity, varchar2(4000), ,
//    start_date, date, ,
//    finish_date, date, ,
//    unit_id, number, y
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @NotNull
    @Column(name = "ind_id", nullable = false)
    private Long indid;
    @NotNull
    @Column(name = "emp_id", nullable = false)
    private Long emp_id;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


