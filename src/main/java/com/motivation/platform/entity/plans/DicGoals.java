package com.motivation.platform.entity.plans;

import com.motivation.platform.config.validator.LastDateOfMonthValidator;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "plans_dic_goals", schema = "MAPP")
public class DicGoals {
//    --OK??
//    id, number(38), ,
//    ind_rule_id, number(38), ,
//    division_code, varchar2(255), ,
//    goal_date, date, ,
//    goal_weight, number, ,
//    goal_owner_sap_post_id, number(38), y,
//    goal_owner_sap_post_name, varchar2(255), ,
//    goal_owner_sap_div_id, number(38), y,
//    goal_owner_sap_div_name, varchar2(255), ,
//    multiplier_code, varchar2(255), y,
//    goal_comments, varchar2(255), y,
//    unit_id, number, y

    @NotBlank
    @Column(name = "division_code", nullable = false)
    public String division_code;
    @NotNull
    @LastDateOfMonthValidator
    @Column(name = "goal_date", nullable = false)
    public java.sql.Date goal_date;
    @NotBlank
    @Column(name = "goal_owner_sap_post_name", nullable = false)
    public String goal_owner_sap_post_name;
    @NotBlank
    @Column(name = "goal_owner_sap_div_name", nullable = false)
    public String goal_owner_sap_div_name;
    @Column(name = "multiplier_code", nullable = true)
    public String multiplier_code;
    @NotNull
    @Column(name = "goal_comments", nullable = false)
    public String goal_comments;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @NotNull
    @Column(name = "ind_rule_id", nullable = false)
    private Long ind_rule_id;
    @NotNull
    @Range(min = 0, max = 1)
    @Column(name = "goal_weight")
    private float goal_weight;
    @NotNull
    @Column(name = "goal_owner_sap_post_id", nullable = false)
    private Long goal_owner_sap_post_id;
    @NotNull
    @Column(name = "goal_owner_sap_div_id", nullable = false)
    private Long goal_owner_sap_div_id;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


