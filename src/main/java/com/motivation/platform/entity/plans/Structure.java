package com.motivation.platform.entity.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "plans_dic_ind_structure_name", schema = "MAPP")
public class Structure {
//    --OK
//  id serial NOT NULL,
//	structure_name varchar(1000) NOT NULL,
//	unit_id int4 NULL,
//	date_add timestamp NULL,
//	add_user_id int4 NULL,
//	date_upd timestamp NULL,
//	upd_user_id int4 NULL

    @NotBlank
    @Column(name = "STRUCTURE_NAME", nullable = false)
    public String structure_name;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


