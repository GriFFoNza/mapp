package com.motivation.platform.entity.plans;

import com.motivation.platform.config.validator.DateRangeValidator;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Data
@Entity
@Table(name = "plans_dic_ind_rules", schema = "MAPP")
public class Rules {
//    --OK???
//    id, number(38), ,
//    indid, number(38), ,
//    ind_detal_level_id, number(38), ,
//    ind_period_type_id, number(38), ,
//    start_date, date, ,
//    finish_date, date, ,
//    unit_id, number, y

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "ind_id", nullable = false)
    private Long indid;

    @ManyToOne
    @JoinColumn(name = "ind_id", insertable=false, updatable=false)
    private Ind ind;

    @NotNull
    @Column(name = "ind_detal_level_id", nullable = false)
    private Long ind_detal_level_id;

    @ManyToOne
    @JoinColumn(name = "ind_detal_level_id", insertable=false, updatable=false)
    private Levels level;

    @NotNull
    @Column(name = "ind_period_type_id", nullable = false)
    private Long ind_period_type_id;

    @ManyToOne
    @JoinColumn(name = "ind_period_type_id", insertable=false, updatable=false)
    private Periods period;

    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


