package com.motivation.platform.entity.plans;

import com.motivation.platform.config.validator.LastDateOfMonthValidator;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "plans_dic_goals_scales", schema = "MAPP")
public class Scales {
//    --OK??
//    id, number(38), ,
//    ind_rule_id, number(38), ,
//    goal_scale_date, date, ,
//    k_estim_min, number, ,
//    k_exec_min, number, ,
//    k_estim_norm, number, ,
//    k_exec_norm, number, ,
//    k_estim_max, number, ,
//    k_exec_max, number, ,
//    k_exec_max_dyn, nvarchar2(50), y,
//    k_estim_max_dyn, nvarchar2(50), y,
//    unit_id, number, y

    @NotNull
    @LastDateOfMonthValidator
    @Column(name = "goal_scale_date", nullable = false)
    public java.sql.Date goal_scale_date;

    @Column(name = "k_exec_max_dyn", nullable = true)
    public String k_exec_max_dyn;

    @Column(name = "k_estim_max_dyn", nullable = true)
    public String k_estim_max_dyn;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "ind_rule_id", nullable = false)
    private Long indruleid;

    @Range(min = 0, max = 2)
    @Column(name = "k_estim_min", nullable = true)
    private float k_estim_min;

    @Range(min = 0, max = 2)
    @Column(name = "k_exec_min", nullable = true)
    private float k_exec_min;

    @Range(min = 0, max = 2)
    @Column(name = "k_estim_norm", nullable = true)
    private float k_estim_norm;

    @Range(min = 0, max = 2)
    @Column(name = "k_exec_norm", nullable = true)
    private float k_exec_norm;

    @Range(min = 0, max = 2)
    @Column(name = "k_estim_max", nullable = true)
    private float k_estim_max;

    @Range(min = 0, max = 2)
    @Column(name = "k_exec_max", nullable = true)
    private float k_exec_max;

    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


