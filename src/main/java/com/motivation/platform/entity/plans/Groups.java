package com.motivation.platform.entity.plans;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "plans_dic_ind_products_groups", schema = "MAPP")
public class Groups extends EntityWithIdAbst {
//    --OK
//    id, number(38), ,
//    list_order_num, number(38), ,
//    list_short_value, varchar2(255), ,
//    list_full_value, varchar2(255), ,
//    unit_id, number,


    @NotNull
    @Column(name = "list_order_num", nullable = false)
    private Long listordernum;

    @NotBlank
    @Column(name = "list_short_value", nullable = false)
    private String list_short_value;

    @NotBlank
    @Column(name = "list_full_value", nullable = false)
    private String list_full_value;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    private Unit unit;

    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


