package com.motivation.platform.entity.plans;

import com.motivation.platform.config.validator.DateRangeValidator;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@DateRangeValidator(DateStart = "start_date", DateEnd = "finish_date")
@Table(name = "plans_dic_ind_scales", schema = "MAPP")
public class IndScales {
//    --OK???
//    id, number(38), ,
//    indid, integer, ,
//    scale_equality_boundary, varchar2(3), ,
//    scale_min_value, number, y,
//    scale_max_value, number, y,
//    scale_result_value, number, ,
//    start_date, date, ,
//    finish_date, date, ,
//    unit_id, number, y

    @NotNull
    @Range(min = 0, max = 100)
    @Column(name = "scale_min_value", nullable = false)
    public float scale_min_value;
    @NotNull
    @Range(min = 0, max = 100)
    @Column(name = "scale_max_value", nullable = false)
    public float scale_max_value;
    @NotNull
    @Range(min = 0, max = 100)
    @Column(name = "scale_result_value", nullable = false)
    public float scale_result_value;
    @NotNull
    @Column(name = "start_date", nullable = false)
    public java.sql.Date start_date;
    @NotNull
    @Column(name = "finish_date", nullable = false)
    public java.sql.Date finish_date;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @NotNull
    @Column(name = "ind_id", nullable = false)
    private Long indid;
    @NotNull
    @Column(name = "scale_equality_boundary", nullable = false)
    private String scale_equality_boundary;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


