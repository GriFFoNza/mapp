package com.motivation.platform.entity.plans;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.entity.FieldName;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "plans_dic_ind", schema = "MAPP")
public class Ind extends EntityWithIdAbst {
//    --OK??
//    id, number(38), ,
//    ind_code, number(38), ,
//    ind_name, varchar2(1000), ,
//    ind_short_name, varchar2(255), ,
//    ind_sale_channel_id, number(38), ,
//    ind_client_segment_id, number(38), ,
//    ind_product_group_id, number(38), ,
//    ind_product_id, number(38), ,
//    ind_exec_type_id, number(38), ,
//    ind_aggr_type_id, number(38), ,
//    ind_measure_id, number(38), ,
//    unit_id, number, y

    @FieldName(name = "Подразделение")
    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    @FieldName(name = "Код")
    @NotNull
    @Column(name = "ind_code", nullable = false)
    private Long ind_code;
    @FieldName(name = "Полное название")
    @NotBlank
    @Column(name = "ind_name", nullable = false)
    private String ind_name;
    @FieldName(name = "Название")
    @NotBlank
    @Column(name = "ind_short_name", nullable = false)
    private String ind_short_name;
    @FieldName(name = "Канал продаж")
    @ManyToOne
    @JoinColumn(name = "ind_sale_channel_id", nullable = true)
    private Channels channel;
    @FieldName(name = "Кл. сегмент")
    @ManyToOne
    @JoinColumn(name = "ind_client_segment_id", nullable = true)
    private Segments segment;
    @FieldName(name = "Группа")
    @ManyToOne
    @JoinColumn(name = "ind_product_group_id", nullable = true)
    private Groups group;
    @FieldName(name = "Продукт")
    @ManyToOne
    @JoinColumn(name = "ind_product_id", nullable = true)
    private Products product;
    @FieldName(name = "Порядок вычисления")
    @ManyToOne
    @JoinColumn(name = "ind_exec_type_id", nullable = true)
    private Exec exec;
    @FieldName(name = "Агрегация")
    @ManyToOne
    @JoinColumn(name = "ind_aggr_type_id", nullable = true)
    private Aggregate aggregate;
    @FieldName(name = "Единица измерения")
    @ManyToOne
    @JoinColumn(name = "ind_measure_id", nullable = true)
    private Measures measure;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;


}


