package com.motivation.platform.entity.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "plans_dic_ind_rules_props", schema = "MAPP")
public class Properties {
//    --OK
//    id, number(38), ,
//    ind_rule_id, number(38), ,
//    flg_is_kpi, number(1), ,
//    flg_is_driver, number(1), ,
//    flg_is_ppr, number(1), ,
//    flg_is_in_bussines_plan, number(1), ,
//    unit_id, number, y

    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @NotNull
    @Column(name = "ind_rule_id", nullable = false)
    private Long ind_rule_id;
    @NotNull
    @Range(min = 0, max = 1)
    @Column(name = "flg_is_kpi", nullable = false)
    private Integer flg_is_kpi;
    @NotNull
    @Range(min = 0, max = 1)
    @Column(name = "flg_is_driver", nullable = false)
    private Integer flg_is_driver;
    @NotNull
    @Range(min = 0, max = 1)
    @Column(name = "flg_is_ppr", nullable = false)
    private Integer flg_is_ppr;
    @NotNull
    @Range(min = 0, max = 1)
    @Column(name = "flg_is_in_bussines_plan", nullable = false)
    private Integer flg_is_in_bussines_plan;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


