package com.motivation.platform.entity.plans;

import com.motivation.platform.config.validator.LastDateOfMonthValidator;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.CreationUserName;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "Plans_calc_sales_plans_vers", schema = "MAPP")
public class SalesVersions {
//    --OK??
//    id serial NOT NULL,
//    ind_rule_id int4 NOT NULL,
//    ind_date timestamp NULL,
//    dt_ins_upd timestamp NULL,
//    login varchar(32) NULL,
//    layer_id int4 NULL,
//    unit_id int4 NULL,
//    date_add timestamp NULL,
//    add_user_id int4 NULL,
//    date_upd timestamp NULL,
//    upd_user_id int4 NULL

    @NotNull
    @LastDateOfMonthValidator
    @Column(name = "ind_date", nullable = false)
    public java.sql.Date ind_date;
    @CreationTimestamp
    @Column(name = "dt_ins_upd", nullable = false)
    public java.sql.Timestamp dt_ins_upd;
    @CreationUserName
    @Column(name = "login", nullable = false)
    public String login;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @NotNull
    @Column(name = "ind_rule_id", nullable = false)
    private Long ind_rule_id;
    @NotNull
    @Column(name = "layer_id", nullable = false)
    private Long layer_id;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


