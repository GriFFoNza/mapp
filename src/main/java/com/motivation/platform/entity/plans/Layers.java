package com.motivation.platform.entity.plans;

import com.motivation.platform.config.validator.DateRangeValidator;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.CreationUserName;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Entity
@DateRangeValidator(DateStart = "start_date", DateEnd = "finish_date")
@Table(name = "plans_dic_layers", schema = "MAPP")
public class Layers {
//    --OK???
//    id, number(38), ,
//    layer_name, varchar2(600), ,
//    start_date, date, ,
//    finish_date, date, ,
//    dt_ins_upd, date, ,
//    login, varchar2(500), ,
//    unit_id, number, y

    @NotBlank
    @Column(name = "layer_name", nullable = false)
    public String layer_name;
    @NotNull
    @Column(name = "start_date", nullable = false)
    public java.sql.Date start_date;
    @NotNull
    @Column(name = "finish_date", nullable = false)
    public java.sql.Date finish_date;
    @CreationTimestamp
    @Column(name = "dt_ins_upd", nullable = false)
    public java.sql.Timestamp dt_ins_upd;
    @CreationUserName
    @Column(name = "login", nullable = false)
    public String login;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit unit;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    private User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    private User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    private java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    private java.sql.Timestamp date_upd;

}


