package com.motivation.platform.entity;

import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "users", schema = "MAPP")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "USERNAME", nullable = false)
    private String username;

    @Column(name = "USERPASS", nullable = false)
    @Size(min = 5, message = "Не меньше 5 знаков")
    private String password;

    @Column(name = "IS_ENABLED", nullable = false, columnDefinition = "INT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean is_enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    //Mapping to Roles table, EAGER must be enabled to avoid errors
    @JoinTable(name = "user_roles", schema = "MAPP",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "v_rolesinherited", schema = "MAPP",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "cur_role_id")
    )
    private Set<Role> rolesInherited;

	/*
	@Override
	@Type(type="true_false") //maps to 0 or 1 in DB
	@Column(name= "LOCKED")
	public boolean isEnabled() {
		return true;
	}
	*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        //this.password = Crypt.encryptPassword(password);
        this.password = password;
    }

    @Override
    public boolean isEnabled() {
        return is_enabled;
    }

    public void setEnabled(boolean is_enabled) {
        this.is_enabled = is_enabled;
    }

    public String isEnabledStr() {
        if (is_enabled) {
            return "Да";
        }
        return "Нет";
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Role> getRolesInherited() {
        return rolesInherited;
    }

    public void setRolesInherited(Set<Role> roles) {
        this.rolesInherited = rolesInherited;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRolesInherited();
    }

    public Collection<? extends GrantedAuthority> getStrictAuthorities() {
        return getRoles();
    }

    @Override
    public String toString() {
        return "ID=" + id + " name=" + username;
    }
}
	/*
	Useful code
	@Transient для исключения полей из маппинга
	@Temporal - для конверсии дат

	FK restrictions
	@Entity
	@Table(name = "App_Role", //
			uniqueConstraints = { //
					@UniqueConstraint(name = "APP_ROLE_UK", columnNames = "Role_Name") })

	//@Transient

	*/

