package com.motivation.platform.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PERMISSIONS", schema = "MAPP")
public class Permission /*implements GrantedAuthority*/ {

    @OneToMany(mappedBy = "permission")
    List<Role> roles;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "PERMISSIONNAME", nullable = false)
    private String permissionname;
    @Column(name = "descr", nullable = false)
    private String descr;

    public String getName() {
        return permissionname;
    }

    public void setName(String name) {
        this.permissionname = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.getName();
    }


}
