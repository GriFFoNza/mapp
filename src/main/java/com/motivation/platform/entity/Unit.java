package com.motivation.platform.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter @Setter
@Entity
@Table(name = "UNITS", schema = "MAPP")
public class Unit extends EntityWithIdAbst {

    @Column(name = "UNITNAME")
    private String unitname;
    @Column(name = "SHORTNAME")
    private String shortname;

    @OneToMany(mappedBy = "unit")
    List<Role> role;
    @OneToMany(mappedBy = "unit")
    List<Goal> goals;
}