package com.motivation.platform.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Immutable
@Table(name = "PG_STAT_ACTIVITY", schema = "MAPP")
public class DB_sessions {

    @Id
    //@Column(name = "PID")
    private Long pid;

    //@Column(name = "USENAME")
    private String backend_start;

    private String state_change;

    //@Column(name = "APPLICATION_NAME")
    private String application_name;

    //@Column(name = "CLIENT_ADDR")
    private String client_addr;

    //@Column(name = "STATE")
    private String state;

    //@Column(name = "QUERY")
    private String query;

    public String getString() {
        return pid + ", " + application_name + ", " + client_addr + ", " + state + ", " + query;
    }

    public Long getpid() {
        return pid;
    }

    public String getbackend_start() {
        return backend_start;
    }

    public String getstate_change() {
        return state_change;
    }

    public String getapplication_name() {
        return application_name;
    }

    public String getclient_addr() {
        return client_addr;
    }

    public String getstate() {
        return state;
    }

    public String getquery() {
        return query;
    }


/*
    public Long getId() {
        return id;
    }

    public String getName() {
        return unitname;
    }

    public String getShortname() {
        return shortname;
    }
*/
}