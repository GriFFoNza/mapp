package com.motivation.platform.entity;

import lombok.Data;
import lombok.Getter;

import javax.persistence.*;


@MappedSuperclass
@Data
public abstract class EntityWithIdAbst {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    protected Long id;

    @Transient
    public static String getDescrFieldName() {
        return "id";
    }


}
