package com.motivation.platform.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T02_UNITS", schema = "MAPP_STG")
public class Unit_t_stg {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "UNITNAME")
    private String unitname;

    @Column(name = "SHORTNAME")
    private String shortname;

    @Column(name = "HASHKEY")
    private String hashkey;

    //public Long getId() {        return id;    }

    public void setId(Long id) {
        this.id = id;
    }


    //public String getName() {        return unitname;    }

    public void setName(String unitname) {
        this.unitname = unitname;
    }

    //public String getShortname() {        return shortname;    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public void setHashkey(String hashkey) {
        this.hashkey = hashkey;
    }
}