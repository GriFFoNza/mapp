package com.motivation.platform.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.motivation.platform.hibernate.CreationUser;
import com.motivation.platform.hibernate.UpdateUser;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

@Getter @Setter
@Entity
@Table(name = "DIC_GOALS", schema = "MAPP")
public class Goal implements HasUnit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    public Long id;

    @Size(min = 1, max = 255)
    @Column(name = "DESCR", nullable = false)
    @FieldName(name = "Описание")
    public String descr;

    @ManyToOne
    @JoinColumn(name = "UNIT_ID", nullable = false)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    //@JsonIdentityReference(alwaysAsId = true)
    @FieldName(name = "Блок")
    public Unit unit;

    @Digits(integer = 20, fraction = 1)
    @Column(name = "VL")
    @FieldName(name = "Значение")
    public float vl;

    @CreationUser
    @ManyToOne
    @JoinColumn(name = "ADD_USER_ID", nullable = false)
    @FieldName(name = "Создал")
    public User user_add;

    @UpdateUser
    @ManyToOne
    @JoinColumn(name = "UPD_USER_ID", nullable = false)
    @FieldName(name = "Обновил")
    public User user_upd;

    @CreationTimestamp
    @Column(name = "DATE_ADD", nullable = false)
    @FieldName(name = "Добавлено")
    public java.sql.Timestamp date_add;

    @UpdateTimestamp
    @Column(name = "DATE_UPD", nullable = false)
    @FieldName(name = "Обновлено")
    public java.sql.Timestamp date_upd;

}


