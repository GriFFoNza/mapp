package com.motivation.platform.entity;

public interface HasUnit {

    Unit getUnit();

    void setUnit(Unit unit);

}
