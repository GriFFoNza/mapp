package com.motivation.platform.entity;

import javax.persistence.*;

@Entity
@Table(name = "T02_UNITS", schema = "MAPP")
public class Unit_t {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "UNITNAME")
    private String unitname;

    @Column(name = "SHORTNAME")
    private String shortname;


    public Long getId() {
        return id;
    }

    public String getName() {
        return unitname;
    }

    public String getShortname() {
        return shortname;
    }

}