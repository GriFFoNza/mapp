package com.motivation.platform.controller;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Set;

//import com.motivation.platform.config.CustomRoleHierarchy;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    public static final String WEB_URL = "/profile";
    public static final String WEB_VIEW_URL = "profile";

    @GetMapping()
    public String users(Model model, Authentication auth) {

        /* Getting current user credentials */
        User currentUser = (User) auth.getPrincipal();
        Set<Role> currentUserRoles = currentUser.getRoles();

        /* Getting current user's access hierarchy */
        Set<Role> userAuthorities = currentUser.getRolesInherited();

        model.addAttribute("currentUserRoles", currentUserRoles);
        model.addAttribute("userAuthorities", userAuthorities);
        model.addAttribute("currentUser", currentUser);
        return WEB_VIEW_URL;

    }

}
