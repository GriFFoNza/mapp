package com.motivation.platform.controller;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.User;
import com.motivation.platform.service.RoleService;
import com.motivation.platform.service.UserService;
import com.motivation.platform.utils.Crypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Validator;

@Controller
@RequestMapping("/users")
@SessionAttributes("user")
public class UserController {

    public static final String WEB_URL = "/users";
    public static final String WEB_VIEW_URL = "users";
    public static final String WEB_FORM_URL = "users_form";
    @Autowired
    Validator validator;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @GetMapping()
    public String users(Model model, HttpServletRequest req) {
        Iterable<User> users = userService.getAll();
        model.addAttribute("users", users);
        return WEB_VIEW_URL;
    }

    @PostMapping()
    public String save(@ModelAttribute("user") User user
            , @RequestParam(required = false) String newPassword
            , BindingResult bindingResult
            , RedirectAttributes redirect) {
        if (!newPassword.isEmpty()) {
            user.setPassword(Crypt.encryptPassword(newPassword));
        }
        userService.save(user);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        return "redirect:" + WEB_URL;
    }


    /*
    // Right correct old way
    @PostMapping()
    public String save (@ModelAttribute("user") User user
            , @RequestParam (required = false) String newPassword
            , RedirectAttributes redirect) {
        if (!newPassword.isEmpty()) {
            user.setPassword(Crypt.encryptPassword(newPassword));
        }
        userService.save(user);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        return "redirect:" + WEB_URL;
    }
     */

    @RequestMapping({"/new", "/edit/{id}"})
    public String add(@PathVariable(required = false) Long id, Model model, HttpServletRequest http) {
        User user = new User();
        if (!http.getServletPath().contains("/new")) {
            user = userService.findById(id);
        }
        Iterable<Role> roles = roleService.getAll();
        model.addAttribute("roles", roles);
        model.addAttribute("user", user);
        return WEB_FORM_URL;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id, Model model, RedirectAttributes redirect) {
        userService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }

}
