package com.motivation.platform.controller;

import com.motivation.platform.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    UserService userService;

    @GetMapping()
    public String login(@RequestParam(value = "error", required = false) String error,
                        @RequestParam(value = "logout", required = false) String logout,
                        HttpServletRequest request,
                        Model model) {

        if (request.getUserPrincipal() != null) {
            return "redirect:/main";
        }

        if (error != null) {
            model.addAttribute("errorMessage", "Неверные логин/пароль");
        } else if (logout != null) {
            model.addAttribute("systemMessage", "Вы вышли");
        } else {
            model.addAttribute("systemMessage", "Для работы с системой необходимо войти");
        }
        return "service/login";
    }

    @GetMapping("/denied")
    public String denied(Model model) {
        return "error/403";
    }
}
