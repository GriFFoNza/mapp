package com.motivation.platform.controller;

import com.motivation.platform.entity.Goal;
import com.motivation.platform.entity.User;
import com.motivation.platform.service.GoalService;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/json/goals")
@SessionAttributes("goal")
public class GoalControllerRest {

    public static final String WEB_URL = "/goals"; //url mapping
    public static final String WEB_VIEW_TEMPLATE = "goals"; //view src
    public static final String WEB_FORM_TEMPLATE = "goals_form"; //add-edit form src
    public static final String AUTHORITY_NAME = "GOAL"; //access name
    @Autowired
    private GoalService goalService;
    @Autowired
    private UnitService unitService;

    //View all records
    @GetMapping()
    @PreAuthorize("hasAuthority('" + AUTHORITY_NAME + "')")
    public Iterable<Goal> goalList(Model model, Principal prn, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "2") Integer size) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<Goal> goals;
        Page<Goal> pageobj;
//        goals = goalService.getByRoles(currentUser.getRolesInherited()); //new
        pageobj = goalService.getByRoles(currentUser.getRolesInherited(), page - 1, size); //new
        return pageobj.getContent();
    }


}

