package com.motivation.platform.controller.plans;

import com.motivation.platform.entity.plans.Levels;
import com.motivation.platform.entity.plans.Periods;
import com.motivation.platform.entity.plans.Rules;
import com.motivation.platform.service.plans.LevelsService;
import com.motivation.platform.service.plans.PeriodsService;
import com.motivation.platform.service.plans.RulesService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/plans/rules")
@SessionAttributes({"rules","ind_rules"})
public class RulesController {

    private final RulesService rulesService;
    private final LevelsService levelsService;
    private final PeriodsService periodsService;

    public static final String WEB_IND_RULES_TEMPLATE = "/plans/indrules";
    public static final String AUTHORITY_NAME = "GOAL";

    public RulesController(RulesService rulesService, LevelsService levelsService, PeriodsService periodsService) {
        this.rulesService = rulesService;
        this.levelsService = levelsService;
        this.periodsService = periodsService;
    }

    @GetMapping("indid/{id}")
    @PreAuthorize("hasAuthority('" + AUTHORITY_NAME + "')")
    public String rulesListByIndId(@PathVariable Long id, Model model) {
        List<Rules> ind_rules = (List<Rules>) rulesService.getByIndId(id);
        Iterable<Levels> ind_detal_levels = levelsService.getAll();
        Iterable<Periods> ind_periods_types = periodsService.getAll();

        Comparator<Rules> rulesComparator = Comparator.comparingLong(Rules::getInd_period_type_id)
                .thenComparingLong(Rules::getInd_detal_level_id);
        ind_rules.sort(rulesComparator);

        model.addAttribute("ind_rules", ind_rules);
        model.addAttribute("indid", id);
        model.addAttribute("ind_periods_types", ind_periods_types);
        model.addAttribute("ind_detal_levels", ind_detal_levels);

        return WEB_IND_RULES_TEMPLATE;
    }
}
