package com.motivation.platform.controller.plans;

import com.motivation.platform.controller.ShortDictControllerRestAbst;
import com.motivation.platform.entity.plans.Channels;
import com.motivation.platform.service.plans.ChannelsServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

@RestController
@RequestMapping("/api/plans/channels")
@SessionAttributes("channels")
public class ChannelsControllerRest extends ShortDictControllerRestAbst<ChannelsServiceImpl, Channels> {
    @Override
    public String getAuth() {
        return "GOAL";
    }

    @Override
    public String getPageTitle() { return "Каналы продаж"; };
}
