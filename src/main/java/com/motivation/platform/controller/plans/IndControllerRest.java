package com.motivation.platform.controller.plans;

import com.motivation.platform.config.ColumnAttributes;
import com.motivation.platform.controller.LongDictControllerRestAbst;
import com.motivation.platform.entity.plans.Ind;
import com.motivation.platform.service.plans.IndServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("/plans/ind")
//@SessionAttributes("ind")
public class IndControllerRest extends LongDictControllerRestAbst<IndServiceImpl, Ind> {
    @Override
    public String getAuth() {
        return "GOAL";
    }

    @Override
    public String[] getViewFieldlist() {
        String[] res = {"id", "ind_code", "ind_short_name", "segment", "channel", "group", "product", "exec", "aggregate", "measure", "unit"};
        return res;
    }

    @Override
    public ColumnAttributes[] getViewFieldAttribute() {
        ColumnAttributes[] res =  {
            new ColumnAttributes("id", false).setHidden(true),
                new ColumnAttributes("ind_code", true),
                new ColumnAttributes("ind_name", true).setHidden(true).setWidthPixel(150),
                new ColumnAttributes("ind_short_name", true).setWidthPixel(300),
                new ColumnAttributes("segment", true),
                new ColumnAttributes("channel", true),
                new ColumnAttributes("group", true),
                new ColumnAttributes("product", true),
                new ColumnAttributes("exec", true),
                new ColumnAttributes("aggregate", true),
                new ColumnAttributes("measure", true),
                new ColumnAttributes("unit", false),
        };
        return res;
    }

    @Override
    public String getPageTitle() { return "Показатели"; };


}
