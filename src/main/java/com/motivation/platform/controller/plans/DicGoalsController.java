package com.motivation.platform.controller.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.entity.plans.DicGoals;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.service.plans.DicGoalsService;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/plans/dicgoals")
@SessionAttributes("dicgoals")
public class DicGoalsController {

    public static final String WEB_URL = "/plans/dicgoals"; //url mapping
    public static final String WEB_VIEW_TEMPLATE = "/plans/dicgoals"; //view src
    public static final String WEB_FORM_TEMPLATE = "/plans/dicgoals_form"; //add-edit form src
    public static final String AUTHORITY_NAME = "GOAL"; //access name
    @Autowired
    private DicGoalsService dicgoalsService;
    @Autowired
    private UnitService unitService;

    //View all records
    @GetMapping()
    @PreAuthorize("hasAuthority('" + AUTHORITY_NAME + "')")
    public String dicgoalsList(Model model, Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<DicGoals> values;
        values = dicgoalsService.getByRoles(currentUser.getRolesInherited()); //new
        model.addAttribute("dicgoals", values);
        return WEB_VIEW_TEMPLATE;
    }

    //Post record
    @PostMapping()
    //@PreAuthorize("hasPermission(#dicgoals,'WRITE')")
    public String save(@Valid @ModelAttribute("dicgoals") DicGoals value
            , BindingResult bindingResult
            , RedirectAttributes redirect
            , SessionStatus sessionStatus) {

        if (bindingResult.hasErrors()) {
            return WEB_FORM_TEMPLATE;
        }
        dicgoalsService.save(value);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }

    //Add or edit record form
    @GetMapping({"/edit/{id}", "/new"})
    //@PreAuthorize("hasPermission('" + AUTHORITY_NAME + "','WRITE')")
    public String edit(@PathVariable(required = false) Long id, Model model, HttpServletRequest http) {
        User currentUser = AuthUtils.getCurrentUser();
        DicGoals value = new DicGoals();
        Iterable<Unit> units = unitService.getUnitsByRole(currentUser.getRolesInherited());
        if (!http.getServletPath().contains("/new")) {
            value = dicgoalsService.getById(id);
        }
        model.addAttribute("dicgoals", value);
        model.addAttribute("units", units);
        return WEB_FORM_TEMPLATE;
    }

    //Delete record
    @GetMapping("/delete/{id}")
    //@PreAuthorize("hasPermission(#id, 'DICGOALS','WRITE')")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        dicgoalsService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }
}
