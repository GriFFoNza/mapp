package com.motivation.platform.controller.plans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/pages/plans/ind")
public class IndControllerAjax {

    @Autowired
    IndControllerRest restController;

    @GetMapping()
    @PreAuthorize("hasAuthority('GOAL')")
    public String get(Model model, Principal prn) {

        String[] fieldsArr = restController.getViewFieldlist();

        String[] EditableFields = {"ind_short_name", "segment", "channel", "group", "product", "exec", "aggregate", "measure", "unit"};

        List<Map<String, String>> columns =
                Arrays.stream(fieldsArr)
                        .map(x -> IntStream.rangeClosed(0, 1).boxed().collect(Collectors.toMap(i -> (new String[] {"field", "title"})[i], i -> (new String[] {x, restController.getFieldCaption(x)})[i])))
                        .collect(Collectors.toList());

        model.addAttribute("title", "Показатели");
        model.addAttribute("columns", columns);
        model.addAttribute("editableColumns", EditableFields);
        model.addAttribute("dropdowns", restController.getAllDropdowns());
        model.addAttribute("ajaxurl", "/api/plans/ind/viewDatatables");
        return "dictajax_datatables";
    }

}
