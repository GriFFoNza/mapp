package com.motivation.platform.controller.plans;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.motivation.platform.entity.plans.Rules;
import com.motivation.platform.entity.plans.Scales;
import com.motivation.platform.service.plans.RulesService;
import com.motivation.platform.service.plans.ScalesService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/plans/rules")
public class RulesControllerRest {

    private final RulesService rulesService;
    private final ScalesService scalesService;

    private static final String AUTHORITY_NAME = "GOAL"; //access name

    public RulesControllerRest(RulesService rulesService, ScalesService scalesService) {
        this.rulesService = rulesService;
        this.scalesService = scalesService;
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('"+AUTHORITY_NAME+"','WRITE')")
    public ResponseEntity<Rules> find (@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<Rules>(rulesService.getById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Rules>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/findrulescales/{id}", method = RequestMethod.GET, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('"+AUTHORITY_NAME+"','WRITE')")
    public ResponseEntity<Iterable<Scales>> findrulescales (@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<Iterable<Scales>>(scalesService.getByIndruleid(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Iterable<Scales>>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/savelist", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,String>> savelist(@RequestBody String json) {
        Map<String,String> response = new HashMap<String, String>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            List<Rules> new_rules = mapper.readValue(json, new TypeReference<List<Rules>>(){});
            List<Rules> old_rules = (List<Rules>) rulesService.getByIndId(new_rules.iterator().next().getIndid());
            for (Rules old_rule :old_rules) {
                new_rules = new_rules.stream().filter(x -> !(x.getInd_period_type_id() == old_rule.getInd_period_type_id() && x.getInd_detal_level_id() == old_rule.getInd_detal_level_id())).collect(Collectors.toList());
            }
            rulesService.saveAll(new_rules);

            response.put("ok", "Success");
            return ResponseEntity.accepted().body(response); }
        catch (DataIntegrityViolationException e)
        {
            String error = e.getCause().getCause().getMessage();
            response.put("error", error);
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            response.put("error", "Error");
            return ResponseEntity.badRequest().body(response);
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,String>> save(@RequestBody String json) {
        Map<String,String> response = new HashMap<String, String>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Rules new_rule = mapper.readValue(json, Rules.class);

            if (new_rule.getId() != null) {
                Rules old_rule = rulesService.getById(new_rule.getId());
                old_rule.setInd_detal_level_id(new_rule.getInd_detal_level_id());
                old_rule.setInd_period_type_id(new_rule.getInd_period_type_id());
                rulesService.save(old_rule);
            } else
            {
                rulesService.save(new_rule);
            }

            response.put("ok", "Success");
            return ResponseEntity.accepted().body(response); }
        catch (DataIntegrityViolationException e)
        {
            String error = e.getCause().getCause().getMessage();
            response.put("error", error);
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            response.put("error", "Error");
            return ResponseEntity.badRequest().body(response);
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,String>> delete(@RequestBody String json) {
        Map<String,String> response = new HashMap<String, String>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(json);
            Long delruleid = jsonNode.get("deleteRuleId").asLong();
            rulesService.deleteById(delruleid);
            response.put("ok", "Success");
            return ResponseEntity.accepted().body(response); }
        catch (DataIntegrityViolationException e)
        {
            String error = e.getCause().getCause().getMessage();
            response.put("error", error);
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            response.put("error", "Error");
            return ResponseEntity.badRequest().body(response);
        }
    }


}
