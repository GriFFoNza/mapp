package com.motivation.platform.controller.plans;

import com.motivation.platform.entity.plans.Measures;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.service.plans.MeasuresService;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.service.plans.MeasuresServiceImpl;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/plans/measures")
@SessionAttributes("measures")
public class MeasuresController {

    @Autowired
    private MeasuresServiceImpl measuresService;
    @Autowired
    private UnitService unitService;

    public static final String WEB_URL = "/plans/measures"; //url mapping
    public static final String WEB_VIEW_TEMPLATE = "/plans/measures"; //view src
    public static final String WEB_FORM_TEMPLATE = "/plans/measures_form"; //add-edit form src
    public static final String AUTHORITY_NAME = "GOAL"; //access name

    //View all records
    @GetMapping()
    @PreAuthorize("hasAuthority('" + AUTHORITY_NAME + "')")
    public String measuresList (Model model, Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<Measures> values;
        values = measuresService.getByRoles(currentUser.getRolesInherited()); //new
        model.addAttribute("measures", values);
        return WEB_VIEW_TEMPLATE;
    }

    //Post record
    @PostMapping()
    //@PreAuthorize("hasPermission(#measures,'WRITE')")
    public String save (@Valid @ModelAttribute("measures")Measures value
            ,BindingResult bindingResult
            ,RedirectAttributes redirect
            ,SessionStatus sessionStatus) {

        if (bindingResult.hasErrors()) {
            return WEB_FORM_TEMPLATE;
        }
        measuresService.save(value);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }

    //Add or edit record form
    @GetMapping({"/edit/{id}", "/new"})
    //@PreAuthorize("hasPermission('" + AUTHORITY_NAME + "','WRITE')")
    public String edit (@PathVariable (required = false) Long id, Model model, HttpServletRequest http) {
        User currentUser = AuthUtils.getCurrentUser();
        Measures value = new Measures();
        Iterable<Unit> units = unitService.getUnitsByRole(currentUser.getRolesInherited());
        if (!http.getServletPath().contains("/new") ) {
            value = measuresService.getById(id);
        }
        model.addAttribute("measures", value);
        model.addAttribute("units", units);
        return WEB_FORM_TEMPLATE;
    }

    //Delete record
    @GetMapping("/delete/{id}")
    //@PreAuthorize("hasPermission(#id, 'MEASURES','WRITE')")
    public String delete (@PathVariable Long id, RedirectAttributes redirect) {
        measuresService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }
}
