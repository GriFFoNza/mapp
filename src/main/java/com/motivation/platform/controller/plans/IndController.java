package com.motivation.platform.controller.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.entity.plans.*;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.service.plans.*;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/plans/ind")
@SessionAttributes("ind")
public class IndController {

    public static final String WEB_URL = "/plans/ind"; //url mapping
    public static final String WEB_VIEW_TEMPLATE = "/plans/ind"; //view src
    public static final String WEB_FORM_TEMPLATE = "/plans/ind_form"; //add-edit form src
    public static final String AUTHORITY_NAME = "GOAL"; //access name
    private final IndServiceImpl indService;
    private final ChannelsServiceImpl channelsService;
    private final SegmentsServiceImpl segmentsService;
    private final ProductsServiceImpl productsService;
    private final GroupsServiceImpl groupsService;
    private final ExecServiceImpl execService;
    private final AggregateServiceImpl aggregateService;
    private final MeasuresServiceImpl measuresService;
    private final UnitService unitService;

    public IndController(IndServiceImpl indService, ChannelsServiceImpl channelsService, SegmentsServiceImpl segmentsService, ProductsServiceImpl productsService, GroupsServiceImpl groupsService, ExecServiceImpl execService, AggregateServiceImpl aggregateService, MeasuresServiceImpl measuresService, UnitService unitService) {
        this.indService = indService;
        this.channelsService = channelsService;
        this.segmentsService = segmentsService;
        this.productsService = productsService;
        this.groupsService = groupsService;
        this.execService = execService;
        this.aggregateService = aggregateService;
        this.measuresService = measuresService;
        this.unitService = unitService;
    }

    //View all records
    @GetMapping()
    @PreAuthorize("hasAuthority('" + AUTHORITY_NAME + "')")
    public String indList(Model model, Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<Ind> values;
        values = indService.getByRoles(currentUser.getRolesInherited()); //new
        model.addAttribute("ind", values);
        return WEB_VIEW_TEMPLATE;
    }

    //Post record
    @PostMapping()
    //@PreAuthorize("hasPermission(#ind,'WRITE')")
    public String save(@Valid @ModelAttribute("ind") Ind value
            , BindingResult bindingResult
            , RedirectAttributes redirect
            , SessionStatus sessionStatus) {

        if (bindingResult.hasErrors()) {
            return WEB_FORM_TEMPLATE;
        }
        indService.save(value);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }

    //Add or edit record form
    @GetMapping({"/edit/{id}", "/new"})
    //@PreAuthorize("hasPermission('" + AUTHORITY_NAME + "','WRITE')")
    public String edit(@PathVariable(required = false) Long id, Model model, HttpServletRequest http) {
        User currentUser = AuthUtils.getCurrentUser();
        Ind value = new Ind();
        Iterable<Channels> channels = channelsService.getByRoles(currentUser.getRolesInherited());
        Iterable<Groups> groups = groupsService.getByRoles(currentUser.getRolesInherited());
        Iterable<Products> products = productsService.getByRoles(currentUser.getRolesInherited());
        Iterable<Segments> segments = segmentsService.getByRoles(currentUser.getRolesInherited());
        Iterable<Exec> execs = execService.getByRoles(currentUser.getRolesInherited());
        Iterable<Aggregate> aggregates = aggregateService.getByRoles(currentUser.getRolesInherited());
        Iterable<Measures> measures = measuresService.getByRoles(currentUser.getRolesInherited());
        Iterable<Unit> units = unitService.getUnitsByRole(currentUser.getRolesInherited());
        if (!http.getServletPath().contains("/new")) {
            value = indService.getById(id);
        }
        model.addAttribute("ind", value);
        model.addAttribute("units", units);
        model.addAttribute("channels", channels);
        model.addAttribute("products", products);
        model.addAttribute("groups", groups);
        model.addAttribute("segments", segments);
        model.addAttribute("measures", measures);
        model.addAttribute("aggregates", aggregates);
        model.addAttribute("execs", execs);
        return WEB_FORM_TEMPLATE;
    }

    //Delete record
    @GetMapping("/delete/{id}")
    //@PreAuthorize("hasPermission(#id, 'IND','WRITE')")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        indService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }
}
