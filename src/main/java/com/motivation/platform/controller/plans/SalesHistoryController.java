package com.motivation.platform.controller.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.entity.plans.SalesHistory;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.service.plans.SalesHistoryService;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/plans/saleshistory")
@SessionAttributes("saleshistory")
public class SalesHistoryController {

    public static final String WEB_URL = "/plans/saleshistory"; //url mapping
    public static final String WEB_VIEW_TEMPLATE = "/plans/saleshistory"; //view src
    public static final String WEB_FORM_TEMPLATE = "/plans/saleshistory_form"; //add-edit form src
    public static final String AUTHORITY_NAME = "GOAL"; //access name
    @Autowired
    private SalesHistoryService saleshistoryService;
    @Autowired
    private UnitService unitService;

    //View all records
    @GetMapping()
    @PreAuthorize("hasAuthority('" + AUTHORITY_NAME + "')")
    public String saleshistoryList(Model model, Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<SalesHistory> values;
        values = saleshistoryService.getByRoles(currentUser.getRolesInherited()); //new
        model.addAttribute("saleshistory", values);
        return WEB_VIEW_TEMPLATE;
    }

    //Post record
    @PostMapping()
    //@PreAuthorize("hasPermission(#saleshistory,'WRITE')")
    public String save(@Valid @ModelAttribute("saleshistory") SalesHistory value
            , BindingResult bindingResult
            , RedirectAttributes redirect
            , SessionStatus sessionStatus) {

        if (bindingResult.hasErrors()) {
            return WEB_FORM_TEMPLATE;
        }
        saleshistoryService.save(value);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }

    //Add or edit record form
    @GetMapping({"/edit/{id}", "/new"})
    //@PreAuthorize("hasPermission('" + AUTHORITY_NAME + "','WRITE')")
    public String edit(@PathVariable(required = false) Long id, Model model, HttpServletRequest http) {
        User currentUser = AuthUtils.getCurrentUser();
        SalesHistory value = new SalesHistory();
        Iterable<Unit> units = unitService.getUnitsByRole(currentUser.getRolesInherited());
        if (!http.getServletPath().contains("/new")) {
            value = saleshistoryService.getById(id);
        }
        model.addAttribute("saleshistory", value);
        model.addAttribute("units", units);
        return WEB_FORM_TEMPLATE;
    }

    //Delete record
    @GetMapping("/delete/{id}")
    //@PreAuthorize("hasPermission(#id, 'SALESHISTORY','WRITE')")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        saleshistoryService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }
}
