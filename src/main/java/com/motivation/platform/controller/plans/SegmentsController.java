package com.motivation.platform.controller.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.entity.plans.Segments;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.service.plans.SegmentsServiceImpl;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/plans/segments")
@SessionAttributes("segments")
public class SegmentsController {

    public static final String WEB_URL = "/plans/segments"; //url mapping
    public static final String WEB_VIEW_TEMPLATE = "/plans/segments"; //view src
    public static final String WEB_FORM_TEMPLATE = "/plans/segments_form"; //add-edit form src
    public static final String AUTHORITY_NAME = "GOAL"; //access name
    @Autowired
    private SegmentsServiceImpl segmentsService;
    @Autowired
    private UnitService unitService;

    //View all records
    @GetMapping()
    @PreAuthorize("hasAuthority('" + AUTHORITY_NAME + "')")
    public String segmentsList(Model model, Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<Segments> values;
        values = segmentsService.getByRoles(currentUser.getRolesInherited()); //new
        model.addAttribute("segments", values);
        return WEB_VIEW_TEMPLATE;
    }

    //Post record
    @PostMapping()
    //@PreAuthorize("hasPermission(#segments,'WRITE')")
    public String save(@Valid @ModelAttribute("segments") Segments value
            , BindingResult bindingResult
            , RedirectAttributes redirect
            , SessionStatus sessionStatus) {

        if (bindingResult.hasErrors()) {
            return WEB_FORM_TEMPLATE;
        }
        segmentsService.save(value);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }

    //Add or edit record form
    @GetMapping({"/edit/{id}", "/new"})
    //@PreAuthorize("hasPermission('" + AUTHORITY_NAME + "','WRITE')")
    public String edit(@PathVariable(required = false) Long id, Model model, HttpServletRequest http) {
        User currentUser = AuthUtils.getCurrentUser();
        Segments value = new Segments();
        Iterable<Unit> units = unitService.getUnitsByRole(currentUser.getRolesInherited());
        if (!http.getServletPath().contains("/new")) {
            value = segmentsService.getById(id);
        }
        model.addAttribute("segments", value);
        model.addAttribute("units", units);
        return WEB_FORM_TEMPLATE;
    }

    //Delete record
    @GetMapping("/delete/{id}")
    //@PreAuthorize("hasPermission(#id, 'SEGMENTS','WRITE')")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        segmentsService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }
}
