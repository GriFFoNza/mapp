package com.motivation.platform.controller;

import com.motivation.platform.config.ColumnAttributes;
import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.entity.User;
import com.motivation.platform.service.BaseServiceAbst;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class BaseDictControllerRestAbst<T extends BaseServiceAbst, E extends EntityWithIdAbst>  {
//    @Autowired
//    protected UnitService unitService;

    @Autowired
    protected T aService;


    protected ColumnAttributes[] fieldsAttrArray;
    protected String[] fieldsList;

    public abstract String getAuth();

    protected void initFieldsArray() {
        fieldsAttrArray = getViewFieldAttribute();
        Arrays.stream(fieldsAttrArray)
                .forEach(x -> x.setCaption(aService.getFieldCaption(x.getField())));
        fieldsList = Arrays.stream(fieldsAttrArray)
                .map(x -> x.getField())
                .collect(Collectors.toList())
                .toArray(new String[0])
        ;
    }

    @GetMapping("/page")
    public String getWebPage(Model model) {
        User currentUser = AuthUtils.getCurrentUser();
        if (fieldsAttrArray == null) {
            initFieldsArray();
        }

        model.addAttribute("title", getPageTitle());
        model.addAttribute("columns", fieldsAttrArray);
        model.addAttribute("dropdowns", aService.getAllDropdowns());
        model.addAttribute("ajaxurl", "viewDatatables");
        return "dictajax_datatables";
//        return "dictajax";
    }

    @GetMapping("/pagetabulator")
    public String getWebPageTabulator(Model model) {
        User currentUser = AuthUtils.getCurrentUser();
        if (fieldsAttrArray == null) {
            initFieldsArray();
        }

        model.addAttribute("title", getPageTitle());
        model.addAttribute("columns", fieldsAttrArray);
        model.addAttribute("dropdowns", aService.getAllDropdowns());
        model.addAttribute("ajaxurl", "viewData");
//        return "dictajax_datatables";
        return "dictajax";
    }


    @ResponseBody
    @GetMapping("/viewData")
    @PreAuthorize("hasAuthority(this.getAuth())")
    public Page<Object[]> getViewData (@RequestParam(required=false, defaultValue = "1") Integer page,
                                       @RequestParam(required = false, defaultValue = "5") Integer size,
                                       @RequestParam(name="sort", required = false) String sort,
                                       @RequestParam(name="dir", required = false) String dir,
                                       @RequestParam(name="search", required = false) String search,
                                       @RequestParam(name="draw", required = false, defaultValue = "0") Integer draw,
                                       @RequestParam(required = false) Map<String, String> filter,
                                       Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        System.out.println(filter);
//        return aService.getJsonData(getViewFieldlist(), null, 0, 0);
        return aService.getJsonDataPage(getViewFieldlist(), false, null, PageRequest.of(page - 1, size), search, sort, dir);
    }

    @ResponseBody
    @GetMapping("/viewDatatables")
    @PreAuthorize("hasAuthority(this.getAuth())")
    public DatatablesWrapper getViewDataTables (@RequestParam(required=false, defaultValue = "1") Integer page,
                                       @RequestParam(required = false, defaultValue = "5") Integer size,
                                       @RequestParam(name="sort", required = false) String sort,
                                       @RequestParam(name="dir", required = false) String dir,
                                       @RequestParam(name="search", required = false) String search,
                                       @RequestParam(name="draw", required = false, defaultValue = "0") Integer draw,
                                       Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
//        return aService.getJsonData(getViewFieldlist(), null, 0, 0);
        return new DatatablesWrapper(aService.getJsonDataPage(fieldsList, true,null, PageRequest.of(page - 1, size), search, sort, dir), draw);
    }

    @PreAuthorize("hasAuthority(this.getAuth())")
    @ResponseBody
    @PutMapping(value = "/edit/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> put (@PathVariable(name="id") Long id, @RequestBody Map<String, Object> input) {
//        System.out.println(obj);
        User currentUser = AuthUtils.getCurrentUser();
        Map<String, String> res = new HashMap<>();
        try {
            res.put("status", "ok");
            aService.updateFields(id, input);
            return res;
        } catch (Exception e) {
            res.put("status", "error");
            res.put("class", e.getClass().getSimpleName());
            res.put("message", e.getMessage());
            return res;
        }
    }


    @ResponseBody
    @PatchMapping(value = "/edit/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @PreAuthorize("hasAuthority(this.getAuth())")
    public Map<String, String> patch (@PathVariable(name="id") Long id, /*, /*, Principal prn */ @RequestBody Map<String, Object> input) {
        User currentUser = AuthUtils.getCurrentUser();
        Map<String, String> res = new HashMap<>();
        try {
            res.put("status", "ok");
            aService.updateFields(id, input);
            return res;
        } catch (Exception e) {
            res.put("status", "error");
            res.put("class", e.getClass().getSimpleName());
            res.put("message", e.getMessage());
            return res;
        }
    }


    public String[] getViewFieldlist() {
        String[] res = {"id"};
        return res;
    }

    public ColumnAttributes[] getViewFieldAttribute() {
        ColumnAttributes[] res = {
                new ColumnAttributes("id", false)
        };
        return res;
    }

    public abstract String getPageTitle();

    public T getAService() {
        return aService;
    }

    public String getFieldCaption(String fname) {
        return aService.getFieldCaption(fname);
    }

    public Map<String, Map<Integer, String>> getAllDropdowns() {
        return aService.getAllDropdowns();
    }


}
