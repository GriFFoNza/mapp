package com.motivation.platform.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"/", "/main", ""})
public class MainController {

    public static final String WEB_URL = "/main";

    @GetMapping()
    public String mainPage(Model model) {
        model.addAttribute("loginedUser", "Admin");
        return "main";
    }

}
