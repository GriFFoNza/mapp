package com.motivation.platform.controller;

import com.motivation.platform.entity.Permission;
import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.service.PermissionService;
import com.motivation.platform.service.RoleService;
import com.motivation.platform.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/roles")
public class RoleController {

    public static final String WEB_URL = "/roles";
    public static final String WEB_VIEW_URL = "roles";
    public static final String WEB_FORM_URL = "roles_form";

    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private UnitService unitService;

    @GetMapping()
    public String roleList(Model model) {
        Iterable<Role> roles = roleService.getAllByOrderByFullname();
        model.addAttribute("roles", roles);
        return WEB_VIEW_URL;
    }

    @PostMapping()
    public String save(Role role, RedirectAttributes redirect) {
        roleService.save(role);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        return "redirect:" + WEB_URL;
    }

    @GetMapping({"/new", "/edit/{id}"})
    public String add(@PathVariable(required = false) Long id, Model model, HttpServletRequest http) {
        Role role = new Role();
        if (!http.getServletPath().contains("/new")) {
            role = roleService.getById(id);
        }
        Iterable<Permission> permissions = permissionService.getAll();

        Iterable<Unit> units = unitService.getAll();

        String ExcludedRolename = (role.getRolename() == null) ? "" : role.getRolename();
        List parents = new ArrayList<Role>();
        if (!ExcludedRolename.equals("ADMIN")) {
            parents = (List<Role>) roleService.getAllByRolenameNotInOrderByFullname(Collections.singletonList(ExcludedRolename));
        }

        model.addAttribute("role", role);
        model.addAttribute("permissions", permissions);
        model.addAttribute("units", units);
        model.addAttribute("parents", parents);
        return WEB_FORM_URL;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        roleService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }

}
