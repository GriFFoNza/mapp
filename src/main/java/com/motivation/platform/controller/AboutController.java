package com.motivation.platform.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutController {
    public static final String WEB_URL = "/about"; //url mapping
    public static final String WEB_VIEW_URL = "about"; //view src

    @GetMapping(WEB_URL)
    public String aboutview(Model model) {
        model.addAttribute("name", "О системе");
        return WEB_VIEW_URL;
    }
}
