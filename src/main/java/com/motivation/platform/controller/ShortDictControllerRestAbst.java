package com.motivation.platform.controller;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.service.ShortDictServiceAbst;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.Map;

//@RestController
public abstract class ShortDictControllerRestAbst<S extends ShortDictServiceAbst, E extends EntityWithIdAbst> extends BaseDictControllerRestAbst<S, E> {


    @GetMapping("/dropdown")
    @PreAuthorize("hasAuthority(this.getAuth())")
    public Map<Integer, String> getDropdownList(Model model, Principal prn) {
        Map<Integer, String> res = aService.getDropdownList();


        return res;
    }


}
