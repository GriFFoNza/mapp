package com.motivation.platform.controller;

import com.motivation.platform.entity.Unit_t;
import com.motivation.platform.entity.Unit_t_stg;
import com.motivation.platform.excel.Excel2DB;
import com.motivation.platform.excel.ExcelPOIHelper;
import com.motivation.platform.service.Stg2mainService;
import com.motivation.platform.service.Unit_tService;
import com.motivation.platform.utils.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class Unit_tController {

    public static final String WEB_URL = "/unit_t"; //url mapping
    public static final String EXCEL_PROCESSING_URL = WEB_URL + "/excelProcessing";
    public static final String EXCEL_EXPORT_URL = WEB_URL + "/excelExport";
    public static final String WEB_VIEW_URL = "unit_t"; //view src
    public static final String WEB_FORM_URL = "unit_t_form"; //add-edit form src
    //public static final String AUTHORITY_NAME = "GOAL"; //access name

    @Autowired
    private Unit_tService unit_tService;

    @Autowired
    private Stg2mainService stg2mainService;

    @Autowired
    private Excel2DB excel2db;

    //View all records
    @GetMapping(WEB_URL)
    public String unitList(Model model, Principal prn) {
        Iterable<Unit_t> unit_t = unit_tService.getAll();
        model.addAttribute("units", unit_t);
        model.addAttribute("export_excel_url", EXCEL_EXPORT_URL);
        model.addAttribute("excel_url", EXCEL_PROCESSING_URL);
        return WEB_VIEW_URL;
    }

    //Post record
    @PostMapping(WEB_URL)
    public String save(@ModelAttribute("unit") Unit_t_stg unit_t_stg, RedirectAttributes redirect, SessionStatus sessionStatus) {

        //Insert STG
        FileUpload fu = new FileUpload();
        String hashkey = fu.gethash();
        unit_t_stg.setHashkey(hashkey);
        unit_tService.saveStg(unit_t_stg);

        //Run proc
        String upd_message = stg2mainService.db_put_stg2main(hashkey, "merge");

        //View db message
        redirect.addFlashAttribute("systemMessage", upd_message);
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }


    //Get Excel
    @PostMapping(value = EXCEL_EXPORT_URL, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void getExcel(@RequestParam(defaultValue = "units") String filename,
                         @RequestParam(defaultValue = "UNITS") String sheetname,
                         HttpServletResponse response
    ) throws IOException {

        ExcelPOIHelper eh = new ExcelPOIHelper();
        //FileUpload fu = new FileUpload();
        //String hashkey= fu.gethash();
        Iterable<Unit_t> unit_t = unit_tService.getAll();
        Map<Integer, List<String>> expdata = new HashMap<>();

        //header
        Integer row = 0;
        List<String> headerCellList = new ArrayList<>();
        headerCellList.add(0, "ID");
        headerCellList.add(1, "Наименование");
        headerCellList.add(2, "Короткое имя");
        expdata.put(row, headerCellList);
        //expdata
        for (Unit_t unit : unit_t) {
            row++;
            List<String> cellList = new ArrayList<>();
            cellList.add(0, unit.getId().toString());
            cellList.add(1, unit.getName());
            cellList.add(2, unit.getShortname());
            expdata.put(row, cellList);
        }

        ByteArrayOutputStream baos = eh.writeExcel(sheetname, expdata);
        //return file
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-disposition", "attachment; filename=" + filename + ".xlsx");
        response.getOutputStream().write(baos.toByteArray());
        //redirect
        response.setHeader("Refresh", "1; url = " + WEB_URL + ";");
        response.getOutputStream().close();
        response.getOutputStream().flush();
    }


    //Post excel
    //@PostMapping(EXCEL_PROCESSING_URL)
    @RequestMapping(method = RequestMethod.POST, value = EXCEL_PROCESSING_URL)
    public String uploadExcel(Model model, MultipartFile file, RedirectAttributes redirect, SessionStatus sessionStatus) {

        FileUpload fu = new FileUpload();
        String hashkey = fu.gethash();

        //Map<String,String> UploadedFile = fu.loadFile(file , new String[]{"xls","xlsx"},hashkey);
        //model.addAttribute("message", UploadedFile.get("message"));
        //String fileLocation = UploadedFile.get("fileLocation");
          /*      try {
                    model.addAttribute("message", excel2db.Unit_update(fileLocation, hashkey));
                }
                catch(Exception e){ System.out.println("Ошибка загрузки файла:"+e);}

                try {
                     new File(fileLocation).delete();
            }
            catch(Exception ignored){}*/
        try {
            //model.addAttribute("message", excel2db.Unit_update(fileLocation, hashkey));
            model.addAttribute("message", excel2db.Unit_update(file, hashkey));
        } catch (Exception e) {
            System.out.println("Ошибка загрузки файла:" + e);
        }

        //Run proc
        String upd_message = stg2mainService.db_put_stg2main(hashkey, "merge");

        //View db message
        redirect.addFlashAttribute("systemMessage", upd_message);


        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }


    //Add or edit record form
    @GetMapping({WEB_URL + "/edit/{id}", WEB_URL + "/new"})
    public String edit(@PathVariable(required = false) Long id, Model model, HttpServletRequest http) {
        Unit_t unit_t = new Unit_t();
        if (http.getServletPath().startsWith(WEB_URL + "/edit")) {
            unit_t = unit_tService.getById(id);
        }
        model.addAttribute("unit", unit_t);
        return WEB_FORM_URL;
    }

    //Delete record
    @GetMapping(WEB_URL + "/delete/{id}")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        //To STG
        FileUpload fu = new FileUpload();
        String hashkey = fu.gethash();
        Unit_t_stg unit_t_stg = new Unit_t_stg();
        unit_t_stg.setId(id);
        unit_t_stg.setHashkey(hashkey);
        unit_tService.saveStg(unit_t_stg);

        //Run proc (delete from main by stg id)
        String del_message = stg2mainService.db_put_stg2main(hashkey, "delete");

        //unit_tService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена." + del_message);
        return "redirect:" + WEB_URL;
    }

}

