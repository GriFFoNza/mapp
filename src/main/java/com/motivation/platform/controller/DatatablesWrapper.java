package com.motivation.platform.controller;

import org.springframework.data.domain.Page;

public class DatatablesWrapper {
    public Iterable data;
    public Integer draw;
    public Long recordsTotal;
    public Long recordsFiltered;
    public DatatablesWrapper(Page<?> page, Integer draw) {
        this.data = page.getContent();
        this.draw = draw;
        this.recordsTotal = page.getTotalElements();
        this.recordsFiltered = page.getTotalElements();
    }
}
