package com.motivation.platform.controller;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.service.LongDictServiceAbst;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

public abstract class LongDictControllerRestAbst<S extends LongDictServiceAbst, E extends EntityWithIdAbst> extends BaseDictControllerRestAbst<S, E> {

    @GetMapping("/autocomplete")
    @PreAuthorize("hasAuthority(this.getAuth())")
    public Iterable<Object[]> getAutocomplete(@RequestParam(required = true) String substring, Model model, Principal prn) {
        return aService.getAutocomplete(substring);
    }

}
