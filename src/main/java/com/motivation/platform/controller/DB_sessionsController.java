package com.motivation.platform.controller;


import com.motivation.platform.entity.DB_sessions;
import com.motivation.platform.service.DB_sessionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DB_sessionsController {

    public static final String WEB_URL = "/sessions";
    public static final String WEB_VIEW_URL = "dbsessions";


    @Autowired
    private DB_sessionsService db_sessionsService;


    @GetMapping(WEB_URL)
    public String sessions(Model model, HttpServletRequest req) {
        Iterable<DB_sessions> dbsessions = db_sessionsService.getAll();
        //dbsessions.forEach(e -> System.out.println(e.getString()));
        model.addAttribute("dbsessions", dbsessions);
        return WEB_VIEW_URL;
    }


}
