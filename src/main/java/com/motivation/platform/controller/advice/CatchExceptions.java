package com.motivation.platform.controller.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/* Intended for catching global exceptions.
Writes an error to "errorMessage" attribute and then redirects back to referrer
 */

@ControllerAdvice
public class CatchExceptions {

    @ExceptionHandler /* ({ConstraintViolationException.class}) */
    public String getStackTrace(Exception e, HttpServletRequest req, RedirectAttributes red) {
        String redirectUrl = "/error";
        String errorMessage = e.getMessage();
        System.out.println("Serious error" + e.getMessage());
        red.addFlashAttribute("errorMessage", errorMessage);
        return "redirect:" + redirectUrl;
    }
}



