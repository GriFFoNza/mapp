package com.motivation.platform.controller.advice;

import com.motivation.platform.entity.User;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.web.bind.annotation.ModelAttribute;

@org.springframework.web.bind.annotation.ControllerAdvice

//Applies to ALL controllers globally via @ControllerAdvice annotation
public class GetCurrentUser {

    @ModelAttribute("currentUser")
    //Returns User object globally to ALL views
    //Can also be returned through Authentication or Principal objects
    public static User getCurrentuser() {
        return AuthUtils.getCurrentUser();
    }

}
