package com.motivation.platform.controller;

import com.motivation.platform.entity.FieldName;
import com.motivation.platform.entity.Goal;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.service.GoalService;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.security.Principal;
import java.util.HashMap;

@Controller
@RequestMapping("/goals")
@SessionAttributes({"goal", "units"})
public class GoalController {

    private static final String ACTION_EDIT = "Изменить";
    private static final String ACTION_ADD = "Добавить";
    private static final String ACTION_INFO = "Аудит";
    private static final String WEB_NAME = "Цели";
    private static final String WEB_URL = "/goals"; //url mapping
    private static final String WEB_VIEW_TEMPLATE = "goals"; //view src
    private static final String WEB_AUDIT_TEMPLATE = "audit"; // src
    private static final String WEB_FORM_TEMPLATE = "goals_form"; //add-edit form src
    private static final String AUTHORITY_NAME = "GOAL"; //access name
    @Autowired
    private GoalService goalService;
    @Autowired
    private UnitService unitService;

    //View all records
    @GetMapping()
    @PreAuthorize("hasAuthority('" + AUTHORITY_NAME + "')")
    public String goalList(Model model, Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<Goal> goals;
        HashMap<String, String> fieldNames = new HashMap<>();

        goals = goalService.getByRoles(currentUser.getRolesInherited()); //new
        for (Field f : Goal.class.getDeclaredFields()) { // for public only getFields()
            FieldName fieldName = f.getAnnotation(FieldName.class);
            fieldNames.put(f.getName(), (fieldName == null) ? f.getName() : fieldName.name());
//            System.out.println(f.getName() + ":" + ((fieldName == null) ? f.getName() : fieldName.name()));
        }

        model.addAttribute("fieldNames", fieldNames);
        model.addAttribute("goals", goals);
        model.addAttribute("name", WEB_NAME);
        return WEB_VIEW_TEMPLATE;
    }

    //Post record
    @PostMapping()
    @PreAuthorize("hasPermission(#goal,'WRITE')")
    public String save(@Valid @ModelAttribute("goal") Goal goal
            , BindingResult bindingResult
            , RedirectAttributes redirect
            , SessionStatus sessionStatus,
                       Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("action", ACTION_EDIT);
            model.addAttribute("name", WEB_NAME);
            return WEB_FORM_TEMPLATE;
        }
        goalService.save(goal);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }

    //Add or view or edit record form
    @GetMapping({"/edit/{id}", "/new"})
    @PreAuthorize("hasPermission('" + AUTHORITY_NAME + "','WRITE')")
    public String edit(@PathVariable(required = false) Long id, Model model, HttpServletRequest http) {
        User currentUser = AuthUtils.getCurrentUser();
        Goal goal;
        Iterable<Unit> units = unitService.getUnitsByRole(currentUser.getRolesInherited());
        if (http.getServletPath().contains("/edit")) {
            goal = goalService.getById(id);
            model.addAttribute("action", ACTION_EDIT);
        } else {
            goal = new Goal();
            model.addAttribute("action", ACTION_ADD);
        }
        model.addAttribute("name", WEB_NAME);
        model.addAttribute("goal", goal);
        model.addAttribute("units", units);
        return WEB_FORM_TEMPLATE;
    }

    @GetMapping({"/info/{id}"})
//    @PreAuthorize("hasPermission('"+AUTHORITY_NAME+"','READ')")
    public String info(@PathVariable(required = false) Long id, Model model) {
        Goal goal = goalService.getById(id);
        model.addAttribute("action", ACTION_INFO);
        model.addAttribute("name", WEB_NAME);
        model.addAttribute("entity", goal);
        model.addAttribute("url", WEB_URL);
        return WEB_AUDIT_TEMPLATE;
    }

    //Delete record
    @GetMapping("/delete/{id}")
    @PreAuthorize("hasPermission(#id, 'GOAL','WRITE')")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        goalService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }

}

