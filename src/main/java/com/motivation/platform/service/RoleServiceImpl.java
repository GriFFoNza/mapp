package com.motivation.platform.service;

import com.motivation.platform.entity.Role;
import com.motivation.platform.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository rep;

    public Iterable<Role> getAll() {
        return rep.findAll();

    }

    @Override
    public Iterable<Role> getAllByOrderByFullname() {
        return rep.findAllByOrderByFullname();
    }

    public Role save(Role role) {
        String fullname =
                role.getRolename() + '_' + role.getPermission().getName() + '_' + role.getUnit().getShortname();
        role.setFullname(fullname);
        return rep.save(role);
    }

    public Role getById(Long id) {
        Role role = rep.findById(id).orElseThrow(RuntimeException::new);
        //Optional<Role> optrole = rep.findById(id);
        //Role role = optrole.isPresent() ? optrole.get() : null;
        return role;
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }

    public Iterable<String> getDistinctAllRolenames() {
        return rep.getDistinctAllRolenames();
    }

    public Iterable<String> getDistinctRolenamesIn(String rolename) {
        return rep.getDistinctRolenamesIn(rolename);
    }

    public Iterable<String> getDistinctRolenamesNotIn(String rolename) {
        return rep.getDistinctRolenamesNotIn(rolename);
    }

    @Override
    public Iterable<Role> getAllByRolenameNotInOrderByFullname(List<String> rolenames) {
        return rep.findAllByRolenameNotInOrderByFullname(rolenames);
    }

}
