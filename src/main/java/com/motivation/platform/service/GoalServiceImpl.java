package com.motivation.platform.service;

import com.motivation.platform.entity.Goal;
import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.repository.GoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class GoalServiceImpl implements GoalService {

    @Autowired
    private GoalRepository rep;

    public Goal save(Goal goal) {
        return rep.save(goal);
    }

    public Goal getById(Long id) {
        Goal goal = rep.findById(id).orElseThrow(RuntimeException::new);
        return goal;
    }

    public Page<Goal> getByRoles(Set<Role> roles, Integer page, Integer size) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getGoalsByUnitIn(units, PageRequest.of(page, size));
    }


    public Iterable<Goal> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getGoalsByUnitIn(units);
    }

    public Iterable<Goal> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }

}

/* Depriciated code
Optional<Goal> optgoal = rep.findById(id);
Goal goal = optgoal.isPresent() ? optgoal.get() : null;
 */