package com.motivation.platform.service;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.repository.BaseRepositoryWithId;

public interface ShortDictService<T extends EntityWithIdAbst, R extends BaseRepositoryWithId> extends BaseService<T, R> {
    Iterable<Object[]> getDropdownList();


}
