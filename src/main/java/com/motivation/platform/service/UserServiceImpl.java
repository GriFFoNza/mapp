package com.motivation.platform.service;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.User;
import com.motivation.platform.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository rep;

    public User save(User user) {
        return rep.save(user);
    }

    public User findById(Long id) {
        User user = rep.findById(id).orElseThrow(RuntimeException::new);
        return user;
    }

    public Iterable<User> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }

    public Optional<User> findByName(String name) {
        return rep.findUserByUsernameIgnoreCase(name);
    }

    public String getRolesByString(User user) {
        Set<Role> roles = user.getRoles();
        if (roles != null) {
            String str = Arrays.toString(roles.toArray());
            str = str.substring(1, str.length() - 1);
            return str;
        }
        return "null";
    }
}


/* Depriciated code

Goal goal = optgoal.isPresent() ? optgoal.get() : null;

public Optional<User> findById (Long id) {
    return rep.findById(id);

} */