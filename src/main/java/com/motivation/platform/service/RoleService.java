package com.motivation.platform.service;

import com.motivation.platform.entity.Role;

import java.util.List;

public interface RoleService {

    Role save(Role goal);

    Role getById(Long id);

    Iterable<Role> getAll();

    Iterable<Role> getAllByOrderByFullname();

    void deleteById(Long id);

    Iterable<String> getDistinctAllRolenames();

    Iterable<String> getDistinctRolenamesIn(String rolename);

    Iterable<String> getDistinctRolenamesNotIn(String rolename);

    Iterable<Role> getAllByRolenameNotInOrderByFullname(List<String> rolenames);
}
