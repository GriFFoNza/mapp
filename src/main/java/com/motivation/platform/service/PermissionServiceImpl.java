package com.motivation.platform.service;

import com.motivation.platform.entity.Permission;
import com.motivation.platform.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionRepository rep;

    public Permission save(Permission user) {
        return rep.save(user);
    }

    public Permission getById(Long id) {
        Permission permission = rep.findById(id).orElseThrow(RuntimeException::new);
        return permission;
    }

    public Iterable<Permission> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}
