package com.motivation.platform.service;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;

import java.util.Set;

public interface UnitService {

    Unit save(Unit goal);

    Unit getById(Long id);

    Iterable<Unit> getAll();

    void deleteById(Long id);

    Set<Unit> getUnitsByRole(Set<Role> roles);

}
