package com.motivation.platform.service;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;

@Service
public abstract class LongDictServiceAbst<T extends EntityWithIdAbst, R extends BaseRepositoryWithIdUnit<T>> extends BaseServiceAbst<T, R> {


    public Iterable<Object[]> getAutocomplete(String substring) {
        if (substring.length() < 3) {
            return new ArrayList<>();
        }
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Object[]> query = cb.createQuery(Object[].class);
        Root<T> r = query.from(entityClass);
        Expression<String> field2 = getDescriptionField(cb, r);
        query.select(cb.array(r.get("id"), field2));
        Predicate w = (substring != null && substring.length() > 0) ? getFilter(cb, r, substring) : null;
        if (w != null) {
            query.where(w);
        }
        query.orderBy(cb.asc(field2));

        TypedQuery<Object[]> typedQuery = em.createQuery(query);
        return typedQuery.setMaxResults(10).getResultList();
    }


}
