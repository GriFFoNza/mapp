package com.motivation.platform.service;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public abstract class ShortDictServiceAbst<T extends EntityWithIdAbst, R extends BaseRepositoryWithIdUnit<T>> extends BaseServiceAbst<T, R> {

    public Map<Long, String> getDropdownList() {
        CriteriaBuilder cb;
        cb = em.getCriteriaBuilder();
        CriteriaQuery<Object[]> query = cb.createQuery(Object[].class);
        Root<T> r = query.from(entityClass);
//        Metamodel m = em.getMetamodel();
//        EntityType<T> T_ = m.entity(entityClass);
        query.select(cb.array(r.get("id"), getDescriptionField(cb, r)));
        return em.createQuery(query).getResultStream().collect(Collectors.toMap(x -> (Long) x[0], x -> (String) x[1]));
    }

    protected abstract Expression<String> getDescriptionField(CriteriaBuilder cb, Root<T> r);

}
