package com.motivation.platform.service;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.entity.FieldName;
import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public abstract class BaseServiceAbst<T extends EntityWithIdAbst, R extends BaseRepositoryWithIdUnit<T>> implements BaseService<T, R> {

    @Autowired
    protected R rep;

    @Autowired
    protected ApplicationContext context;

    @PersistenceContext
    protected EntityManager em;

    @Autowired
    private EntityManagerFactory emf;

    @Autowired
    protected ServiceByEntityName serviceEntity;

    protected Class<T> entityClass;
    protected Map<String, String> fieldEntity;
    protected Map<String, String> fieldCaption;
    protected Map<String, String> fieldShortDict;

    public enum Ftype {
        TEXT, ID
    }


    public BaseServiceAbst() {
        this.entityClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        fieldEntity = new HashMap<>();
        fieldCaption = new HashMap<>();
        for (Field f: entityClass.getDeclaredFields()) {
            if (EntityWithIdAbst.class.isAssignableFrom(f.getType())) {
                fieldEntity.put(f.getName(), f.getType().getSimpleName());

            }
            FieldName fn = f.getAnnotation(FieldName.class);
            fieldCaption.put(f.getName(), fn!=null ? fn.name() : f.getName());
        }

    }

    public String getFieldCaption(String fname) {
        return fieldCaption.getOrDefault(fname, fname);
    }

    public T save (T value) {
        return rep.save(value);
    }

    public T getById (Long id) {
        T value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<T> getByRoles (Set<Role> roles) {
        Set<Unit> units = new HashSet<>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Page<T> getPage (Set<Role> roles, Integer page, Integer size) {
        Set<Unit> units = new HashSet<>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units, PageRequest.of(page, size));
    }

    public Iterable<T> getAll () {
        return rep.findAll();
    }

    public void deleteById (Long id) {
        rep.deleteById(id);
    }


    protected Path getFieldPath(Root<T> r, String fname, Ftype ftype) {
        Path el =  r.get(fname);
        if (fieldEntity.containsKey(fname)) {
            if (ftype == Ftype.TEXT)
            {
                switch (fieldEntity.get(fname)) {
                    case "Channels":
                    case "Aggregate":
                    case "Products":
                    case "Segments":
                    case "Groups":
                    case "Exec":
                    case "Measures":
                        el = el.get("list_short_value");
                        break;
                    case "Unit":
                        el = el.get("shortname");
                        break;
                    default:
                        el = el.get("id");
                }

            } else if (ftype == Ftype.ID)
                el = el.get("id");
        }
        return el;
    }

    protected Object dynamicCast(String obj, Class<?> Target) throws ClassNotFoundException {
        switch (Target.getSimpleName()) {
            case "Long":
                return Long.parseLong(obj);
            case "Integer":
                return Integer.parseInt(obj);
            case "Double":
                return Double.parseDouble(obj);
            case "Float":
                return Float.parseFloat(obj);
            case "String":
                return obj;
            case "Date":
                return java.sql.Date.valueOf(obj);
            default:
                throw new ClassNotFoundException();
        }
    }

    protected Object dynamicCast(String obj, Path e) throws ClassNotFoundException {
        return dynamicCast(obj, e.getModel().getBindableJavaType());
    }


    @Transactional
    public void updateFields(Long id, Map<String, Object> fields) throws ClassNotFoundException {
        EntityManager localem = emf.createEntityManager();

        // T obj = rep.findById(id).orElseThrow(() -> new RuntimeException("Not found"));
        try {
            CriteriaBuilder cb = localem.getCriteriaBuilder();
            CriteriaUpdate<T> upd = cb.createCriteriaUpdate(entityClass);
            Root<T> r = upd.from(entityClass);
            Integer countFields = 0;

            for (String f : fields.keySet()) {
                try {
                    entityClass.getDeclaredField(f);
                    Path k = getFieldPath(r, f, Ftype.ID);
                    upd.set(k, dynamicCast((String) fields.get(f), k));
                    countFields += 1;
                } catch (NoSuchFieldException e) {}
            }
            if (countFields.equals(0)) return;

            upd.where(cb.equal(r.get("id"), id));
            EntityTransaction et = localem.getTransaction();
            et.begin();
            try {
                Integer count = localem.createQuery(upd).executeUpdate();
                if (count.equals(0)) {
                    throw new EntityNotFoundException("Объект не найден");
                }
                et.commit();
            } catch (Exception e) {
                et.rollback();
                throw e;
            }
        } finally {
            localem.close();
        }

    }

    public Page<Map<String, Object>> getJsonDataPage(String[] fieldList,
                                                     Boolean idForDropdown,
                                                     Set<Role> roles,
                                                     Pageable pageable,
                                                     String search,
                                                     String sort,
                                                     String dir) {
        return new PageImpl<>(getJsonData(fieldList, idForDropdown, roles, (int) pageable.getOffset(), pageable.getPageSize(),  search, sort, dir), pageable, countData(roles, search));
    }

    public Long countData(Set<Role> roles, String search) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Long> query = cb.createQuery(Long.class);
        Root<T> r = query.from(entityClass);
        query.select(cb.count(r));
        Predicate w = (search != null && search.length() > 0) ? getFilter(cb, r, search) : null;
        if (w != null) {
            query.where(w);
        }
        return em.createQuery(query).getSingleResult();
    }

    public List<Map<String, Object>> getJsonData(String[] fieldList, Boolean idForDropdown, Set<Role> roles, Integer offset, Integer limit, String search, String sort, String dir) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<Object[]> query = cb.createQuery(Object[].class);
        Root<T> r = query.from(entityClass);
//        Expression[] sel = new Expression[fieldList.size()];



        List<Expression> sel = new ArrayList<>();
        for (String fieldName: fieldList) {
            BaseServiceAbst serv;
            serv = serviceEntity.getService(fieldEntity.getOrDefault(fieldName, ""));
            if (idForDropdown && serv != null && ShortDictServiceAbst.class.isAssignableFrom(serv.getClass())) {
                sel.add(getFieldPath(r, fieldName, Ftype.ID));
            } else {
                sel.add(getFieldPath(r, fieldName, Ftype.TEXT));
            }
        }
        query.select(cb.array(sel.toArray(new Expression[0])));

        Predicate w = (search != null && search.length() > 0) ? getFilter(cb, r, search) : null;
        if (w != null) {
            query.where(w);
        }

        if (sort != null && sort.length() > 0) {
            String sort0 = sort.split(",")[0];
            String dir0 = dir.split(",")[0];
            Path ordExpr = getFieldPath(r, sort0, Ftype.TEXT);
            if (dir0.equals("asc")) {
                query.orderBy(cb.asc(ordExpr));
            } else {
                query.orderBy(cb.desc(ordExpr));
            }
        }

        TypedQuery<Object[]> typedQuery = em.createQuery(query);
        if (limit != null && limit > 0) {
            if (offset != null && offset > 0) {
                typedQuery.setFirstResult(offset);
            }
            typedQuery.setMaxResults(limit);
        }
        return typedQuery.getResultStream()
                .map(x -> IntStream.range(0, x.length).boxed().collect(Collectors.toMap(i -> fieldList[i], i -> x[i])))
                .collect(Collectors.toList());
    }

    protected Expression<String> getDescriptionField(CriteriaBuilder cb, Root<T> r) {
        return r.get("id").as(String.class);
    }

    protected Predicate getFilter(CriteriaBuilder cb, Root<T> r, String substring) {
        return null;
    }

    public Map<String, Map<Integer, String>> getAllDropdowns() {
        Map<String, Map<Integer, String>> dd = new HashMap<>();
        BaseServiceAbst serv;
        for (Field f: entityClass.getDeclaredFields()) {
            if (EntityWithIdAbst.class.isAssignableFrom(f.getType())) {
                serv = serviceEntity.getService(f.getType().getSimpleName());
                if (serv != null) {
                    if (ShortDictServiceAbst.class.isAssignableFrom(serv.getClass())) {
                        dd.put(f.getName(), ((ShortDictServiceAbst) serv).getDropdownList());

                    }
                }
//                context.getBeansOfType(ResolvableType.forClassWithGenerics(Short));
            }
        }
        return dd;
    }


}
