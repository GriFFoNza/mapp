package com.motivation.platform.service;

import com.motivation.platform.entity.Unit_t;
import com.motivation.platform.entity.Unit_t_stg;

public interface Unit_tService {

    //Unit_t save(Unit_t goal);
    Unit_t getById(Long id);

    Iterable<Unit_t> getAll();

    void deleteById(Long id);

    //STG
    Unit_t_stg saveStg(Unit_t_stg goal);

    Unit_t_stg getByIdStg(Long id);

    Iterable<Unit_t_stg> getAllstg();

}
