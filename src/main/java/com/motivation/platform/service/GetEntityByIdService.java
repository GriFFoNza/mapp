package com.motivation.platform.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/*
Returns entity object by id and class name
 */

@Service
public class GetEntityByIdService {

    static String entityClassPath = "com.motivation.platform.entity.";
    @PersistenceContext
    EntityManager entityManager;

    public Object getEntity(String className, Long id) {

        String classToQuery = StringUtils.capitalize(className.toLowerCase());
        System.out.println(this.getClass().getSimpleName().concat(" >> ") + "got classname: " + className);

        /* should test camelcase */
        /* Check whether the class is Entity to decide whether to query DB or not */
        try {
            System.out.println(entityClassPath + classToQuery);
            Class cls = Class.forName(entityClassPath + classToQuery);
        } catch (Exception e) {
            System.out.println(this.getClass().getSimpleName().concat(" >> ") + "not matches with any entity");
            return null;
        }

        /* Query DB if the class is Entity */
        Query query;
        try {
            query = entityManager.createQuery("SELECT Q FROM " + classToQuery + " Q WHERE Q.id=?1");
            query.setParameter(1, id);
            return query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
