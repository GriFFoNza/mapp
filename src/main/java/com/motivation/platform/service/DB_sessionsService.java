package com.motivation.platform.service;


import com.motivation.platform.entity.DB_sessions;

public interface DB_sessionsService {

    Iterable<DB_sessions> getAll();

}
