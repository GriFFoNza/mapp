package com.motivation.platform.service;

import com.motivation.platform.entity.Unit_t;
import com.motivation.platform.entity.Unit_t_stg;
import com.motivation.platform.repository.Unit_tRepository;
import com.motivation.platform.repository.Unit_t_stgRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class Unit_tServiceImpl implements Unit_tService {

    @Autowired
    private Unit_tRepository rep;

    @Autowired
    private Unit_t_stgRepository rep_stg;


    public Unit_t getById(Long id) {
        Unit_t unit_t = new Unit_t();
        try {
            unit_t = rep.findById(id).orElseThrow(RuntimeException::new);
        } catch (Exception e) {
            //unit_t.setId(id);
            System.out.println("Строка не найдена по id = " + id);
        }
        return unit_t;
    }

    public Unit_t_stg getByIdStg(Long id) {
        Unit_t_stg unit_t_stg = new Unit_t_stg();
       /* try {
            unit_t_stg = rep_stg.findById(id).orElseThrow(RuntimeException::new);
        } catch(Exception e){*/
        unit_t_stg.setId(id);
        //}
        return unit_t_stg;
    }

    public Iterable<Unit_t> getAll() {
        return rep.findAll();
    }

    public Iterable<Unit_t_stg> getAllstg() {
        return rep_stg.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }

    public Unit_t_stg saveStg(Unit_t_stg user) {
        return rep_stg.save(user);
    }

}
