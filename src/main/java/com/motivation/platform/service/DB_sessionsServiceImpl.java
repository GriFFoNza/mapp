package com.motivation.platform.service;

import com.motivation.platform.entity.DB_sessions;
import com.motivation.platform.repository.DB_SessionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DB_sessionsServiceImpl implements DB_sessionsService {

    @Autowired
    private DB_SessionsRepository rep;

    public Iterable<DB_sessions> getAll() {
        return rep.findAll();
    }


}
