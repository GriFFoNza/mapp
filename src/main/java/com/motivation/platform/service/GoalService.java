package com.motivation.platform.service;

import com.motivation.platform.entity.Goal;
import com.motivation.platform.entity.Role;
import org.springframework.data.domain.Page;

import java.util.Set;

public interface GoalService {

    Goal save(Goal goal);

    Goal getById(Long id);

    Iterable<Goal> getByRoles(Set<Role> roles);

    Page<Goal> getByRoles(Set<Role> roles, Integer page, Integer size);

    Iterable<Goal> getAll();

    void deleteById(Long id);

}
