package com.motivation.platform.service;


import com.motivation.platform.service.plans.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ServiceByEntityName {

    @Autowired
    protected ChannelsServiceImpl channelsService;
    @Autowired
    protected AggregateServiceImpl aggregateService;
    @Autowired
    protected IndServiceImpl indService;
    @Autowired
    protected ExecServiceImpl execService;
    @Autowired
    protected GroupsServiceImpl groupService;
    @Autowired
    protected ProductsServiceImpl productService;
    @Autowired
    protected SegmentsServiceImpl segmentService;
    @Autowired
    protected MeasuresServiceImpl measureService;

    protected Map<String, BaseServiceAbst> spr;


    public BaseServiceAbst getService(String input) {
        if (spr == null) {
            spr = new HashMap<>();
            spr.put("Channels", channelsService);
            spr.put("Aggregate", aggregateService);
            spr.put("Ind", indService);
            spr.put("Exec", execService);
            spr.put("Groups", groupService);
            spr.put("Products", productService);
            spr.put("Segments", segmentService);
            spr.put("Measures", measureService);
        }
        if (spr.containsKey(input)) return spr.get(input);
        return null;


    }

}
