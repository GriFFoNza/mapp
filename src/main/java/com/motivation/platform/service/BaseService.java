package com.motivation.platform.service;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.entity.Role;
import com.motivation.platform.repository.BaseRepositoryWithId;

import java.util.Set;

public interface BaseService<T extends EntityWithIdAbst, R extends BaseRepositoryWithId> {

    T save(T _dicgoals);

    T getById(Long id);

    Iterable<T> getByRoles(Set<Role> roles);

    Iterable<T> getAll();

    void deleteById(Long id);

}
