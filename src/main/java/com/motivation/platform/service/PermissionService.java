package com.motivation.platform.service;

import com.motivation.platform.entity.Permission;


public interface PermissionService {

    Permission save(Permission permission);

    Permission getById(Long id);

    Iterable<Permission> getAll();

    void deleteById(Long id);

}
