package com.motivation.platform.service;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.repository.UnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitRepository rep;

    public Unit save(Unit unit) {
        return rep.save(unit);
    }

    public Unit getById(Long id) {
        Unit unit = rep.findById(id).orElseThrow(RuntimeException::new);
        return unit;
    }

    public Iterable<Unit> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }

    public Set<Unit> getUnitsByRole(Set<Role> roles) {
        return rep.getUnitByRoleIn(roles);
    }
}
