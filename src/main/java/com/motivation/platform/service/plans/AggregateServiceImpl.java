package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Aggregate;
import com.motivation.platform.repository.plans.AggregateRepository;
import com.motivation.platform.service.ShortDictServiceAbst;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

@Service
public class AggregateServiceImpl extends ShortDictServiceAbst<Aggregate, AggregateRepository> {
    @Override
    protected Expression<String> getDescriptionField(CriteriaBuilder cb, Root<Aggregate> r) {
        return r.get("list_short_value");
    }
}
