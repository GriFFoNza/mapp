package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.StructureType;

import java.util.Set;

public interface StructureTypeService {

    StructureType save(StructureType _structuretype);

    StructureType getById(Long id);

    Iterable<StructureType> getByRoles(Set<Role> roles);

    Iterable<StructureType> getAll();

    void deleteById(Long id);
}

