package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Groups;
import com.motivation.platform.entity.plans.Products;
import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.repository.plans.GroupsRepository;
import com.motivation.platform.repository.plans.ProductsRepository;
import com.motivation.platform.service.BaseServiceAbst;
import com.motivation.platform.service.ShortDictServiceAbst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.HashSet;
import java.util.Set;

@Service
public class ProductsServiceImpl extends ShortDictServiceAbst<Products, ProductsRepository> {
    @Override
    protected Expression<String> getDescriptionField(CriteriaBuilder cb, Root<Products> r) {
        return r.get("list_short_value");
    }
}
