package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Structure;
import com.motivation.platform.repository.plans.StructureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class StructureServiceImpl implements StructureService {

    @Autowired
    private StructureRepository rep;

    public Structure save(Structure value) {
        return rep.save(value);
    }

    public Structure getById(Long id) {
        Structure value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<Structure> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<Structure> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }

}

