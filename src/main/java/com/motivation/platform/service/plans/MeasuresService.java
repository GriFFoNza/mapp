package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Measures;

import java.util.Set;

public interface MeasuresService {

    Measures save(Measures _measures);

    Measures getById(Long id);

    Iterable<Measures> getByRoles(Set<Role> roles);

    Iterable<Measures> getAll();

    void deleteById(Long id);

}

