package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.IndScales;
import com.motivation.platform.repository.plans.IndScalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class IndScalesServiceImpl implements IndScalesService {

    @Autowired
    private IndScalesRepository rep;

    public IndScales save(IndScales value) {
        return rep.save(value);
    }

    public IndScales getById(Long id) {
        IndScales value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<IndScales> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<IndScales> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}

