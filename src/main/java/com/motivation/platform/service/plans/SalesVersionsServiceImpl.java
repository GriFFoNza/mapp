package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.SalesVersions;
import com.motivation.platform.repository.plans.SalesVersionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class SalesVersionsServiceImpl implements SalesVersionsService {

    @Autowired
    private SalesVersionsRepository rep;

    public SalesVersions save(SalesVersions value) {
        return rep.save(value);
    }

    public SalesVersions getById(Long id) {
        SalesVersions value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<SalesVersions> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<SalesVersions> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}