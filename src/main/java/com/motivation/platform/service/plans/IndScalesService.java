package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.IndScales;

import java.util.Set;

public interface IndScalesService {

    IndScales save(IndScales _indscales);

    IndScales getById(Long id);

    Iterable<IndScales> getByRoles(Set<Role> roles);

    Iterable<IndScales> getAll();

    void deleteById(Long id);
}

