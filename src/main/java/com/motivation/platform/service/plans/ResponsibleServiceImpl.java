package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Responsible;
import com.motivation.platform.repository.plans.ResponsibleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ResponsibleServiceImpl implements ResponsibleService {

    @Autowired
    private ResponsibleRepository rep;

    public Responsible save(Responsible value) {
        return rep.save(value);
    }

    public Responsible getById(Long id) {
        Responsible value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<Responsible> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<Responsible> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}

