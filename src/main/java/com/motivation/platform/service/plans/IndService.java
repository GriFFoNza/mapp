package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Ind;

import java.util.Set;

public interface IndService {

    Ind save(Ind _ind);

    Ind getById(Long id);

    Iterable<Ind> getByRoles(Set<Role> roles);

    Iterable<Ind> getAll();

    void deleteById(Long id);
}

