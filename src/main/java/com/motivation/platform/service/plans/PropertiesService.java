package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Properties;

import java.util.Set;

public interface PropertiesService {

    Properties save(Properties _properties);

    Properties getById(Long id);

    Iterable<Properties> getByRoles(Set<Role> roles);

    Iterable<Properties> getAll();

    void deleteById(Long id);
}

