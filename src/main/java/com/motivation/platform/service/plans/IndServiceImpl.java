package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Ind;
import com.motivation.platform.repository.plans.IndRepository;
import com.motivation.platform.service.LongDictServiceAbst;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Service
public class IndServiceImpl extends LongDictServiceAbst<Ind, IndRepository> {
    @Override
    protected Expression<String> getDescriptionField(CriteriaBuilder cb, Root<Ind> r) {
        return cb.concat(cb.concat(r.get("ind_code"), " "), r.get("ind_short_name"));
    }

    @Override
    protected Predicate getFilter(CriteriaBuilder cb, Root<Ind> r, String substring) {
        substring = "%" + substring.toLowerCase() + "%";
        return cb.or(cb.like(r.get("ind_code").as(String.class), substring), cb.like(cb.lower(r.get("ind_short_name")), substring), cb.like(cb.lower(r.get("ind_name")), substring));
    }


}

