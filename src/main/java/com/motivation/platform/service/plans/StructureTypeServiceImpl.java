package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.StructureType;
import com.motivation.platform.repository.plans.StructureTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class StructureTypeServiceImpl implements StructureTypeService {

    @Autowired
    private StructureTypeRepository rep;

    public StructureType save(StructureType value) {
        return rep.save(value);
    }

    public StructureType getById(Long id) {
        StructureType value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<StructureType> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<StructureType> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }

}

