package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Levels;

import java.util.Set;

public interface LevelsService {

    Levels save(Levels _levels);

    Levels getById(Long id);

    Iterable<Levels> getByRoles(Set<Role> roles);

    Iterable<Levels> getAll();

    void deleteById(Long id);
}

