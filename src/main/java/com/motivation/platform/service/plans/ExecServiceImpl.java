package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Exec;
import com.motivation.platform.repository.plans.ExecRepository;
import com.motivation.platform.service.ShortDictServiceAbst;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

@Service
public class ExecServiceImpl extends ShortDictServiceAbst<Exec, ExecRepository> {
    @Override
    protected Expression<String> getDescriptionField(CriteriaBuilder cb, Root<Exec> r) {
        return r.get("list_short_value");
    }
}

