package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Sales;

import java.util.Set;

public interface SalesService {

    Sales save(Sales _dicgoals);

    Sales getById(Long id);

    Iterable<Sales> getByRoles(Set<Role> roles);

    Iterable<Sales> getAll();

    void deleteById(Long id);
}
