package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Levels;
import com.motivation.platform.repository.plans.LevelsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class LevelsServiceImpl implements LevelsService {

    @Autowired
    private LevelsRepository rep;

    public Levels save(Levels value) {
        return rep.save(value);
    }

    public Levels getById(Long id) {
        Levels value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<Levels> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<Levels> getAll() {
        return rep.findAllByOrderByListordernumAsc();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}


