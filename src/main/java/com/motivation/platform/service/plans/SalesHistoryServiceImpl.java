package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.SalesHistory;
import com.motivation.platform.repository.plans.SalesHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class SalesHistoryServiceImpl implements SalesHistoryService {

    @Autowired
    private SalesHistoryRepository rep;

    public SalesHistory save(SalesHistory value) {
        return rep.save(value);
    }

    public SalesHistory getById(Long id) {
        SalesHistory value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<SalesHistory> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<SalesHistory> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}