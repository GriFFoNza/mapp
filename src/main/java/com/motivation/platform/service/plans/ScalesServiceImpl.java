package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Scales;
import com.motivation.platform.repository.plans.ScalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ScalesServiceImpl implements ScalesService {

    @Autowired
    private ScalesRepository rep;

    public Scales save(Scales value) {
        return rep.save(value);
    }

    public Scales getById(Long id) {
        Scales value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<Scales> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<Scales> getAll() {
        return rep.findAll();
    }

    public Iterable<Scales> getByIndruleid(Long id) {
        return rep.getByIndruleid(id);
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}
