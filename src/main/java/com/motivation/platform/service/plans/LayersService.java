package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Layers;

import java.util.Set;

public interface LayersService {

    Layers save(Layers _layers);

    Layers getById(Long id);

    Iterable<Layers> getByRoles(Set<Role> roles);

    Iterable<Layers> getAll();

    void deleteById(Long id);

}

