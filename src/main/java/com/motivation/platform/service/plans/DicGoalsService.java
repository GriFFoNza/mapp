package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.DicGoals;

import java.util.Set;

public interface DicGoalsService {

    DicGoals save(DicGoals _dicgoals);

    DicGoals getById(Long id);

    Iterable<DicGoals> getByRoles(Set<Role> roles);

    Iterable<DicGoals> getAll();

    void deleteById(Long id);
}
