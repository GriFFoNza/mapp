package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Structure;

import java.util.Set;

public interface StructureService {

    Structure save(Structure _structure);

    Structure getById(Long id);

    Iterable<Structure> getByRoles(Set<Role> roles);

    Iterable<Structure> getAll();

    void deleteById(Long id);
}

