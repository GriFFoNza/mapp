package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Segments;
import com.motivation.platform.repository.plans.SegmentsRepository;
import com.motivation.platform.service.ShortDictServiceAbst;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

@Service
public class SegmentsServiceImpl extends ShortDictServiceAbst<Segments, SegmentsRepository> {
    @Override
    protected Expression<String> getDescriptionField(CriteriaBuilder cb, Root<Segments> r) {
        return r.get("list_short_value");
    }
}
