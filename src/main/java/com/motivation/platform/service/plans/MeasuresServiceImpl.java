package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Measures;
import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.repository.plans.MeasuresRepository;
import com.motivation.platform.service.ShortDictServiceAbst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.HashSet;
import java.util.Set;

@Service
public class MeasuresServiceImpl extends ShortDictServiceAbst<Measures, MeasuresRepository> {

    @Override
    protected Expression<String> getDescriptionField(CriteriaBuilder cb, Root<Measures> r) {
        return r.get("list_short_value");
    }
}

