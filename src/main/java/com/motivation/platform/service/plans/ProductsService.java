package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Products;

import java.util.Set;

public interface ProductsService {

    Products save(Products _products);

    Products getById(Long id);

    Iterable<Products> getByRoles(Set<Role> roles);

    Iterable<Products> getAll();

    void deleteById(Long id);
}

