package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Rules;
import com.motivation.platform.repository.plans.RulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RulesServiceImpl implements RulesService {

    @Autowired
    private RulesRepository rep;

    public Rules save(Rules value) {
        return rep.save(value);
    }

    public Iterable<Rules> saveAll(Iterable<Rules> value) {
        return rep.saveAll(value);
    }

    public Rules getById(Long id) {
        Rules value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    /*public Iterable<Rules> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }*/

    @Override
    public Iterable<Rules> getByIndId(Long id) {
        return rep.getByIndid(id);
    }

    public Iterable<Rules> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}

