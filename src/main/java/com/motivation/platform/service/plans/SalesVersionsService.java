package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.SalesVersions;

import java.util.Set;

public interface SalesVersionsService {

    SalesVersions save(SalesVersions _dicgoals_vers);

    SalesVersions getById(Long id);

    Iterable<SalesVersions> getByRoles(Set<Role> roles);

    Iterable<SalesVersions> getAll();

    void deleteById(Long id);
}
