package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Groups;

import java.util.Set;

public interface GroupsService {

    Groups save(Groups _groups);

    Groups getById(Long id);

    Iterable<Groups> getByRoles(Set<Role> roles);

    Iterable<Groups> getAll();

    void deleteById(Long id);
}


