package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Exec;

import java.util.Set;

public interface ExecService {

    Exec save(Exec _exec);

    Exec getById(Long id);

    Iterable<Exec> getByRoles(Set<Role> roles);

    Iterable<Exec> getAll();

    void deleteById(Long id);
}

