package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.SalesHistory;

import java.util.Set;

public interface SalesHistoryService {

    SalesHistory save(SalesHistory _dicgoals_hst);

    SalesHistory getById(Long id);

    Iterable<SalesHistory> getByRoles(Set<Role> roles);

    Iterable<SalesHistory> getAll();

    void deleteById(Long id);
}
