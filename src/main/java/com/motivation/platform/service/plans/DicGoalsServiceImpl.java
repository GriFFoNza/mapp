package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.DicGoals;
import com.motivation.platform.repository.plans.DicGoalsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class DicGoalsServiceImpl implements DicGoalsService {

    @Autowired
    private DicGoalsRepository rep;

    public DicGoals save(DicGoals value) {
        return rep.save(value);
    }

    public DicGoals getById(Long id) {
        DicGoals value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<DicGoals> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<DicGoals> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}