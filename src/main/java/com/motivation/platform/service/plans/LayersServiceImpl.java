package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Layers;
import com.motivation.platform.repository.plans.LayersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class LayersServiceImpl implements LayersService {

    @Autowired
    private LayersRepository rep;

    public Layers save(Layers value) {
        return rep.save(value);
    }

    public Layers getById(Long id) {
        Layers value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<Layers> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<Layers> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }

}

