package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Periods;
import com.motivation.platform.repository.plans.PeriodsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class PeriodsServiceImpl implements PeriodsService {

    @Autowired
    private PeriodsRepository rep;

    public Periods save(Periods value) {
        return rep.save(value);
    }

    public Periods getById(Long id) {
        Periods value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<Periods> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<Periods> getAll() {
        return rep.findAllByOrderByListordernumAsc();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}

