package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Aggregate;
import com.motivation.platform.repository.plans.AggregateRepository;
import com.motivation.platform.service.BaseService;

public interface AggregateService extends BaseService<Aggregate, AggregateRepository> {

}

