package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Channels;
import com.motivation.platform.repository.plans.ChannelsRepository;
import com.motivation.platform.service.ShortDictServiceAbst;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

@Service
public class ChannelsServiceImpl extends ShortDictServiceAbst<Channels, ChannelsRepository> /*implements ChannelsService*/ {
    @Override
    protected Expression<String> getDescriptionField(CriteriaBuilder cb, Root<Channels> r) {
        return r.get("list_short_value");
    }

}

