package com.motivation.platform.service.plans;

import com.motivation.platform.entity.plans.Channels;
import com.motivation.platform.repository.plans.ChannelsRepository;
import com.motivation.platform.service.ShortDictService;

public interface ChannelsService extends ShortDictService<Channels, ChannelsRepository> {

}

