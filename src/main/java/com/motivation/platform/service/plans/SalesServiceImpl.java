package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Sales;
import com.motivation.platform.repository.plans.SalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class SalesServiceImpl implements SalesService {

    @Autowired
    private SalesRepository rep;

    public Sales save(Sales value) {
        return rep.save(value);
    }

    public Sales getById(Long id) {
        Sales value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<Sales> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<Sales> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}