package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Properties;
import com.motivation.platform.repository.plans.PropertiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class PropertiesServiceImpl implements PropertiesService {

    @Autowired
    private PropertiesRepository rep;

    public Properties save(Properties value) {
        return rep.save(value);
    }

    public Properties getById(Long id) {
        Properties value = rep.findById(id).orElseThrow(RuntimeException::new);
        return value;
    }

    public Iterable<Properties> getByRoles(Set<Role> roles) {
        Set<Unit> units = new HashSet<Unit>();
        roles.forEach(e -> {
            units.add(e.getUnit());
        });
        return rep.getByUnitIn(units);
    }

    public Iterable<Properties> getAll() {
        return rep.findAll();
    }

    public void deleteById(Long id) {
        rep.deleteById(id);
    }
}

