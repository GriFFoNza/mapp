package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Rules;

import java.util.Set;

public interface RulesService {

    Rules save(Rules _rules);

    Iterable<Rules> saveAll(Iterable<Rules> rules);

    Rules getById(Long id);

    /*Iterable<Rules> getByRoles(Set<Role> roles);*/

    Iterable<Rules> getByIndId(Long id);

    Iterable<Rules> getAll();

    void deleteById(Long id);
}

