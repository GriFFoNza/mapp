package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Responsible;

import java.util.Set;

public interface ResponsibleService {

    Responsible save(Responsible _responsible);

    Responsible getById(Long id);

    Iterable<Responsible> getByRoles(Set<Role> roles);

    Iterable<Responsible> getAll();

    void deleteById(Long id);

}

