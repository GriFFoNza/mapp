package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Segments;

import java.util.Set;

public interface SegmentsService {

    Segments save(Segments _segments);

    Segments getById(Long id);

    Iterable<Segments> getByRoles(Set<Role> roles);

    Iterable<Segments> getAll();

    void deleteById(Long id);

}

