package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Scales;

import java.util.Set;

public interface ScalesService {

    Scales save(Scales _scales);

    Scales getById(Long id);

    Iterable<Scales> getByRoles(Set<Role> roles);

    Iterable<Scales> getAll();

    Iterable<Scales> getByIndruleid(Long id);

    void deleteById(Long id);
}