package com.motivation.platform.service.plans;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.plans.Periods;

import java.util.Set;

public interface PeriodsService {

    Periods save(Periods _periods);

    Periods getById(Long id);

    Iterable<Periods> getByRoles(Set<Role> roles);

    Iterable<Periods> getAll();

    void deleteById(Long id);
}

