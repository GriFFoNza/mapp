package com.motivation.platform.service;

import com.motivation.platform.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserService userService;
    @Autowired
    RoleServiceImpl roleService;

    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Optional<User> getUser = userService.findByName(userName);
        if (!getUser.isPresent()) {
            throw new UsernameNotFoundException("Username not found");
        }
        User currentUser = getUser.get();

        System.out.println(this.getClass().getSimpleName().concat(" >> ")
                + "User authorized:" + currentUser.getUsername());

        currentUser.getRoles().forEach(e -> {
            System.out.println(this.getClass().getSimpleName().concat(" >> ") + " User's rights " + e.getFullname());
        });

        return currentUser;
    }
}

