package com.motivation.platform.service;


import com.motivation.platform.repository.Stg2mainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class Stg2mainServiceImpl implements Stg2mainService {

    @Autowired
    private Stg2mainRepository stg2MAIN_repository;

    public String db_put_stg2main(String vhashkey, String vdo_action) {
        return stg2MAIN_repository.put_stg2main(vhashkey, vdo_action);
    }

}
