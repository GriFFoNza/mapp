package com.motivation.platform.service;

import com.motivation.platform.entity.User;

import java.util.Optional;

public interface UserService {

    User save(User user);

    //Optional<User> findById (Long id);
    User findById(Long id);

    Optional<User> findByName(String name);

    Iterable<User> getAll();

    void deleteById(Long id);

    String getRolesByString(User usr);

}
