package com.motivation.platform.repository;

import com.motivation.platform.entity.EntityWithIdAbst;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepositoryWithId<T extends EntityWithIdAbst> extends CrudRepository<T, Long> {
}
