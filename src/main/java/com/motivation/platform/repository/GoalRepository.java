package com.motivation.platform.repository;

import com.motivation.platform.entity.Goal;
import com.motivation.platform.entity.Unit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface GoalRepository extends CrudRepository<Goal, Long> {

    /* Iterable<Goal> getGoalsByRoles (Set<Role> roles); */
    Iterable<Goal> getGoalsByUnit(Unit unit);

    Iterable<Goal> getGoalsByUnitIn(Set<Unit> unit);

    Page<Goal> getGoalsByUnitIn(Set<Unit> unit, Pageable pageable);
}
