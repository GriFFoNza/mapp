package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Rules;
import org.springframework.data.repository.CrudRepository;


public interface RulesRepository extends CrudRepository<Rules, Long> {

/*    Iterable<Rules> getByUnit(Unit unit);

    Iterable<Rules> getByUnitIn(Set<Unit> unit);*/

    Iterable<Rules> getByIndid(Long id);
}
