package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Segments;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;

public interface SegmentsRepository extends BaseRepositoryWithIdUnit<Segments> {
}
