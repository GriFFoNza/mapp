package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.DicGoals;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface DicGoalsRepository extends CrudRepository<DicGoals, Long> {

    Iterable<DicGoals> getByUnit(Unit unit);

    Iterable<DicGoals> getByUnitIn(Set<Unit> unit);
}
