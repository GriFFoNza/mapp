package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.IndScales;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface IndScalesRepository extends CrudRepository<IndScales, Long> {

    Iterable<IndScales> getByUnit(Unit unit);

    Iterable<IndScales> getByUnitIn(Set<Unit> unit);
}
