package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Groups;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;

public interface GroupsRepository extends BaseRepositoryWithIdUnit<Groups> {
}
