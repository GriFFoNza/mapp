package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Aggregate;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;

public interface AggregateRepository extends BaseRepositoryWithIdUnit<Aggregate> {
}
