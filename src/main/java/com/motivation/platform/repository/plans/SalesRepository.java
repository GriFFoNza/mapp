package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Sales;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface SalesRepository extends CrudRepository<Sales, Long> {

    Iterable<Sales> getByUnit(Unit unit);

    Iterable<Sales> getByUnitIn(Set<Unit> unit);
}
