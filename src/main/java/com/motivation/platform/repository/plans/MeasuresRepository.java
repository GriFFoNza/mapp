package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Measures;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface MeasuresRepository extends BaseRepositoryWithIdUnit<Measures> {

    Iterable<Measures> getByUnit(Unit unit);
    Iterable<Measures> getByUnitIn(Set<Unit> unit);
}
