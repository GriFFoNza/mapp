package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Ind;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;

public interface IndRepository extends BaseRepositoryWithIdUnit<Ind> {
}
