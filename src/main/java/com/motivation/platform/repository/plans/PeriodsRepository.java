package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Periods;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface PeriodsRepository extends CrudRepository<Periods, Long> {

    Iterable<Periods> getByUnit(Unit unit);

    Iterable<Periods> getByUnitIn(Set<Unit> unit);

    Iterable<Periods> findAllByOrderByListordernumAsc();
}
