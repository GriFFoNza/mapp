package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Layers;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface LayersRepository extends CrudRepository<Layers, Long> {

    Iterable<Layers> getByUnit(Unit unit);

    Iterable<Layers> getByUnitIn(Set<Unit> unit);

}
