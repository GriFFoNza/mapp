package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Properties;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface PropertiesRepository extends CrudRepository<Properties, Long> {

    Iterable<Properties> getByUnit(Unit unit);

    Iterable<Properties> getByUnitIn(Set<Unit> unit);
}
