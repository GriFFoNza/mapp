package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Scales;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface ScalesRepository extends CrudRepository<Scales, Long> {

    Iterable<Scales> getByUnit(Unit unit);

    Iterable<Scales> getByUnitIn(Set<Unit> unit);

    Iterable<Scales> getByIndruleid(Long id);
}
