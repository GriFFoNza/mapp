package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Responsible;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface ResponsibleRepository extends CrudRepository<Responsible, Long> {

    Iterable<Responsible> getByUnit(Unit unit);

    Iterable<Responsible> getByUnitIn(Set<Unit> unit);
}
