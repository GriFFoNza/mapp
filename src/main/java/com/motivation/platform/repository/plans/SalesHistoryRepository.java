package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.SalesHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface SalesHistoryRepository extends CrudRepository<SalesHistory, Long> {

    Iterable<SalesHistory> getByUnit(Unit unit);

    Iterable<SalesHistory> getByUnitIn(Set<Unit> unit);
}
