package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.StructureType;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface StructureTypeRepository extends CrudRepository<StructureType, Long> {

    Iterable<StructureType> getByUnit(Unit unit);

    Iterable<StructureType> getByUnitIn(Set<Unit> unit);
}
