package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.SalesVersions;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface SalesVersionsRepository extends CrudRepository<SalesVersions, Long> {

    Iterable<SalesVersions> getByUnit(Unit unit);

    Iterable<SalesVersions> getByUnitIn(Set<Unit> unit);
}
