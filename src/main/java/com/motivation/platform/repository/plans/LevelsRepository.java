package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Levels;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface LevelsRepository extends CrudRepository<Levels, Long> {

    Iterable<Levels> getByUnit(Unit unit);

    Iterable<Levels> getByUnitIn(Set<Unit> unit);

    Iterable<Levels> findAllByOrderByListordernumAsc();
}
