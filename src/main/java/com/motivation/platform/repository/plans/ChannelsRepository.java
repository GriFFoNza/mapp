package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Channels;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;

public interface ChannelsRepository extends BaseRepositoryWithIdUnit<Channels> {
}
