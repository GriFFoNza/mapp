package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Exec;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;

public interface ExecRepository extends BaseRepositoryWithIdUnit<Exec> {
}
