package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.plans.Products;
import com.motivation.platform.repository.BaseRepositoryWithIdUnit;

public interface ProductsRepository extends BaseRepositoryWithIdUnit<Products> {
}
