package com.motivation.platform.repository.plans;

import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.plans.Structure;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface StructureRepository extends CrudRepository<Structure, Long> {

    Iterable<Structure> getByUnit(Unit unit);

    Iterable<Structure> getByUnitIn(Set<Unit> unit);
}
