package com.motivation.platform.repository;

import com.motivation.platform.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    //Don't use access to repository directly (bad pattern)

    // For custom SQL queries:
    // @Query("select b from Bank b where b.name = :name")
    //Bank findByName(@Param("name") String name);

    Optional<User> findUserByUsernameIgnoreCase(String name);

}
