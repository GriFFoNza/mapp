package com.motivation.platform.repository;

import com.motivation.platform.entity.EntityWithIdAbst;
import com.motivation.platform.entity.Unit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Set;

@NoRepositoryBean
public interface BaseRepositoryWithIdUnit<T extends EntityWithIdAbst> extends BaseRepositoryWithId<T> {
    Iterable<T> getByUnit(Unit unit);

    Iterable<T> getByUnitIn(Set<Unit> unit);

    Page<T> getByUnitIn(Set<Unit> unit, Pageable pageable);

}
