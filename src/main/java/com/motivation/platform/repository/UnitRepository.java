package com.motivation.platform.repository;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface UnitRepository extends CrudRepository<Unit, Long> {
    Set<Unit> getUnitByRoleIn(Set<Role> roles);
}
