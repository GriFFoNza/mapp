package com.motivation.platform.repository;

import com.motivation.platform.entity.Unit_t;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Stg2mainRepository extends JpaRepository<Unit_t, String> {

    @Query(value = "CALL mapp.p_stg2main(:hashkey,:do_action);", nativeQuery = true)
    String put_stg2main(@Param("hashkey") String vhaskey, @Param("do_action") String vdo_action);
}
