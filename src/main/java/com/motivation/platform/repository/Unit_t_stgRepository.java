package com.motivation.platform.repository;

import com.motivation.platform.entity.Unit_t_stg;
import org.springframework.data.repository.CrudRepository;

public interface Unit_t_stgRepository extends CrudRepository<Unit_t_stg, Long> {

}
