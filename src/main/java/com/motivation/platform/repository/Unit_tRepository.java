package com.motivation.platform.repository;

import com.motivation.platform.entity.Unit_t;
import org.springframework.data.repository.CrudRepository;

public interface Unit_tRepository extends CrudRepository<Unit_t, Long> {

}
