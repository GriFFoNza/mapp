package com.motivation.platform.repository;

import com.motivation.platform.entity.DB_sessions;
import org.springframework.data.repository.CrudRepository;

public interface DB_SessionsRepository extends CrudRepository<DB_sessions, Long> {

}
