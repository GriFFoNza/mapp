package com.motivation.platform.repository;


import com.motivation.platform.entity.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleRepository extends CrudRepository<Role, Long> {

    //Get all distinct rolenames
    @Query("SELECT DISTINCT a.rolename FROM Role a")
    Iterable<String> getDistinctAllRolenames();

    //Get distinct rolenames except particular rolename
    @Query("SELECT DISTINCT a.rolename FROM Role a WHERE a.rolename <> ?1")
    Iterable<String> getDistinctRolenamesNotIn(String name);

    //Get distinct rolenames filtered by particular rolename
    @Query("SELECT DISTINCT a.rolename FROM Role a WHERE a.rolename <> ?1")
    Iterable<String> getDistinctRolenamesIn(String name);

    Iterable<Role> findAllByRolenameNotInOrderByFullname(List<String> rolenames);

    Iterable<Role> findAllByOrderByFullname();
}
