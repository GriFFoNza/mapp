package com.motivation.platform.excel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.IntStream;


/*StreamingReader reader = StreamingReader.builder()
        .rowCacheSize(100)    // number of rows to keep in memory (defaults to 10)
        .bufferSize(4096)     // buffer size to use when reading InputStream to file (defaults to 1024)
        .sheetIndex(0)        // index of sheet to use (defaults to 0)
        .read(is);            // InputStream or File for XLSX file (required)

for (Row r : reader) {
  for (Cell c : r) {
    System.out.println(c.getStringCellValue());
  }
} */


public class ExcelPOIHelper {


    public Map<Integer, List<MyCell>> readExcel(String fileLocation) throws IOException {

        Map<Integer, List<MyCell>> data = new HashMap<>();
        //FileInputStream fis = new FileInputStream(new File(fileLocation));

        InputStream is = new DataInputStream(new FileInputStream(new File(fileLocation)));


        if (fileLocation.endsWith(".xls")) {
            data = readHSSFWorkbook(is);
        } else if (fileLocation.endsWith(".xlsx")) {
            data = readXSSFWorkbook(is);
        }

        int maxNrCols = maxCols(data);

        data.values().stream()
                .filter(ls -> ls.size() < maxNrCols)
                .forEach(ls -> {
                    IntStream.range(ls.size(), maxNrCols)
                            .forEach(i -> ls.add(new MyCell("")));
                });

        return data;
    }

    public Map<Integer, List<MyCell>> readExcel(MultipartFile file) throws IOException {

        Map<Integer, List<MyCell>> data = new HashMap<>();
        InputStream is = new BufferedInputStream(file.getInputStream());
        if (file.getOriginalFilename() != null) {
            if (file.getOriginalFilename().endsWith(".xls")) {
                data = readHSSFWorkbook(is);
            } else if (file.getOriginalFilename().endsWith(".xlsx")) {
                data = readXSSFWorkbook(is);
            }
        } else {
            return null;
        }

        int maxNrCols = maxCols(data);

        data.values().stream()
                .filter(ls -> ls.size() < maxNrCols)
                .forEach(ls -> {
                    IntStream.range(ls.size(), maxNrCols)
                            .forEach(i -> ls.add(new MyCell("")));
                });

        return data;
    }

    private int maxCols(Map<Integer, List<MyCell>> data) {
        return data.values().stream()
                .mapToInt(List::size)
                .max()
                .orElse(0);
    }


    private String readCellContent(Cell cell) {
        String content;
        switch (cell.getCellType()) {
            case STRING:
                content = cell.getStringCellValue();
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date;
                    date = cell.getDateCellValue();
                    content = sdf.format(date);
                } else {
                    java.text.NumberFormat nf = java.text.NumberFormat.getInstance();
                    nf.setGroupingUsed(false);
                    content = String.valueOf(nf.format(cell.getNumericCellValue()));
                }
                break;
            case BOOLEAN:
                content = cell.getBooleanCellValue() + "";
                break;
            case FORMULA:
                content = cell.getCellFormula() + "";
                break;
            default:
                content = "";
        }
        return content;
    }

   /* private Map<Integer, List<MyCell>> readHSSFWorkbook(FileInputStream fis) throws IOException {
        Map<Integer, List<MyCell>> data = new HashMap<>();
        HSSFWorkbook workbook = null;
        try {
            workbook = new HSSFWorkbook(fis);

            HSSFSheet sheet = workbook.getSheetAt(0);
            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                HSSFRow row = sheet.getRow(i);
                data.put(i, new ArrayList<>());
                if (row != null) {
                    for (int j = 0; j < row.getLastCellNum(); j++) {
                        HSSFCell cell = row.getCell(j);
                        if (cell != null) {

                            MyCell myCell = new MyCell();

                            myCell.setContent(readCellContent(cell));
                            data.get(i)
                                .add(myCell);
                        } else {
                            data.get(i)
                                .add(new MyCell(""));
                        }
                    }
                }
            }
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return data;
    }*/

    private Map<Integer, List<MyCell>> readHSSFWorkbook(InputStream is) throws IOException {
        Map<Integer, List<MyCell>> data = new HashMap<>();
        HSSFWorkbook workbook = null;
        try {
            workbook = new HSSFWorkbook(is);

            HSSFSheet sheet = workbook.getSheetAt(0);
            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                HSSFRow row = sheet.getRow(i);
                data.put(i, new ArrayList<>());
                if (row != null) {
                    for (int j = 0; j < row.getLastCellNum(); j++) {
                        HSSFCell cell = row.getCell(j);
                        if (cell != null) {

                            MyCell myCell = new MyCell();

                            myCell.setContent(readCellContent(cell));
                            data.get(i)
                                    .add(myCell);
                        } else {
                            data.get(i)
                                    .add(new MyCell(""));
                        }
                    }
                }
            }
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return data;
    }

   /* private Map<Integer, List<MyCell>> readXSSFWorkbook(FileInputStream fis) throws IOException {
            XSSFWorkbook workbook = null;
            Map<Integer, List<MyCell>> data = new HashMap<>();
            try {

            workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);

            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                XSSFRow row = sheet.getRow(i);
                data.put(i, new ArrayList<>());
                if (row != null) {
                    for (int j = 0; j < row.getLastCellNum(); j++) {
                        XSSFCell cell = row.getCell(j);
                        if (cell != null) {

                            MyCell myCell = new MyCell();

                            myCell.setContent(readCellContent(cell));
                            data.get(i)
                                .add(myCell);
                        } else {
                            data.get(i)
                                .add(new MyCell(""));
                        }
                    }
                }
            }
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return data;
    }*/

    private Map<Integer, List<MyCell>> readXSSFWorkbook(InputStream is) throws IOException {
        XSSFWorkbook workbook = null;
        Map<Integer, List<MyCell>> data = new HashMap<>();
        try {

            workbook = new XSSFWorkbook(is);
            XSSFSheet sheet = workbook.getSheetAt(0);

            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                XSSFRow row = sheet.getRow(i);
                data.put(i, new ArrayList<>());
                if (row != null) {
                    for (int j = 0; j < row.getLastCellNum(); j++) {
                        XSSFCell cell = row.getCell(j);
                        if (cell != null) {

                            MyCell myCell = new MyCell();

                            myCell.setContent(readCellContent(cell));
                            data.get(i)
                                    .add(myCell);
                        } else {
                            data.get(i)
                                    .add(new MyCell(""));
                        }
                    }
                }
            }
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return data;
    }

    public ByteArrayOutputStream writeExcel(String sheetname, Map<Integer, List<String>> expdata) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook();

        try {

            Sheet sheet = workbook.createSheet(sheetname);
            /*Styles*/
            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Calibri");
            headerFont.setFontHeightInPoints((short) 12);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);
            CellStyle style = workbook.createCellStyle();
            style.setWrapText(true);
            /*/Styles*/

            Row row;
            for (Map.Entry<Integer, List<String>> rowdata : expdata.entrySet()) {
                Integer rownum = rowdata.getKey();
                List<String> cells = rowdata.getValue();
                Cell headerCell;
                row = sheet.createRow(rownum);

                for (int i = 0; i < cells.size(); i++) {
                    headerCell = row.createCell(i);
                    headerCell.setCellValue(cells.get(i));
                    if (rownum == 0) {
                        headerCell.setCellStyle(headerStyle);
                        sheet.autoSizeColumn(i);
                    } else {
                        headerCell.setCellStyle(style);
                    }
                }

            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            workbook.write(baos);
            //FileOutputStream outputStream = new FileOutputStream(fileLocation);
            //workbook.write(outputStream);
            return baos;
        } finally {
            workbook.close();
        }
    }

}