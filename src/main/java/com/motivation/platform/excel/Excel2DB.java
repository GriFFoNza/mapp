package com.motivation.platform.excel;

import com.motivation.platform.entity.Unit_t_stg;
import com.motivation.platform.service.Unit_tService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public class Excel2DB {

    @Resource(name = "excelPOIHelper")
    private ExcelPOIHelper excelPOIHelper;


    @Autowired
    private Unit_tService unit_tService;

    public String Unit_update(MultipartFile file, String hashkey) throws IOException {
        String ret_message = "";

        if (file.getOriginalFilename().endsWith(".xlsx") || file.getOriginalFilename().endsWith(".xls")) {
            //Map<Integer, List<MyCell>> data = excelPOIHelper.readExcel(fileLocation);
            Map<Integer, List<MyCell>> data = excelPOIHelper.readExcel(file);
            /*Загрузка*/
            new Unit_t_stg();
            Unit_t_stg write_unit;
            for (Map.Entry<Integer, List<MyCell>> entry : data.entrySet()) {
                Integer rownum = entry.getKey();
                List<MyCell> row = entry.getValue();
                if (rownum > 0) {
                    try {
                        write_unit = unit_tService.getByIdStg(Long.parseLong(row.get(0).getContent()));
                        //write_unit.Id(row.get(0).getContent());
                        write_unit.setName(row.get(1).getContent());
                        write_unit.setShortname(row.get(2).getContent());
                        write_unit.setHashkey(hashkey);
                        unit_tService.saveStg(write_unit);
                    } catch (Exception e) {
                        StringBuilder print_e = new StringBuilder();
                        for (MyCell coll : row) {
                            print_e.append(coll.getContent()).append(" ");
                        }
                        System.out.println(print_e + " " + e);
                    }
                }
            }
            ret_message = "Файл загружен";

            //model.addAttribute("data", data);
        } else {

            ret_message = "Не валидный файл!";
        }

        return ret_message;
    }
}
