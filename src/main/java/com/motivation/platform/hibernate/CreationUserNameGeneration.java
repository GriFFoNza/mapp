package com.motivation.platform.hibernate;

import com.motivation.platform.utils.AuthUtils;
import org.hibernate.Session;
import org.hibernate.tuple.AnnotationValueGeneration;
import org.hibernate.tuple.GenerationTiming;
import org.hibernate.tuple.ValueGenerator;

public class CreationUserNameGeneration implements AnnotationValueGeneration<CreationUserName> {

    private final ValueGenerator<String> generator = new ValueGenerator<String>() {
        @Override
        public String generateValue(Session session, Object o) {
            return AuthUtils.getCurrentUser().getUsername();
        }
    };

    @Override
    public void initialize(CreationUserName annotation, Class<?> propertyType) {

    }

    @Override
    public GenerationTiming getGenerationTiming() {
        return GenerationTiming.INSERT;
    }

    @Override
    public ValueGenerator<?> getValueGenerator() {
        return generator;
    }

    @Override
    public boolean referenceColumnInSql() {
        return false;
    }

    @Override
    public String getDatabaseGeneratedReferencedColumnValue() {
        return null;
    }
}