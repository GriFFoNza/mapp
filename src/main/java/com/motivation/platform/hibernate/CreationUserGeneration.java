package com.motivation.platform.hibernate;

import com.motivation.platform.entity.User;
import com.motivation.platform.utils.AuthUtils;
import org.hibernate.Session;
import org.hibernate.tuple.AnnotationValueGeneration;
import org.hibernate.tuple.GenerationTiming;
import org.hibernate.tuple.ValueGenerator;

public class CreationUserGeneration implements AnnotationValueGeneration<CreationUser> {

    private final ValueGenerator<User> generator = new ValueGenerator<User>() {
        @Override
        public User generateValue(Session session, Object o) {
            return AuthUtils.getCurrentUser();
        }
    };

    @Override
    public void initialize(CreationUser annotation, Class<?> propertyType) {

    }

    @Override
    public GenerationTiming getGenerationTiming() {
        return GenerationTiming.INSERT;
    }

    @Override
    public ValueGenerator<?> getValueGenerator() {
        return generator;
    }

    @Override
    public boolean referenceColumnInSql() {
        return false;
    }

    @Override
    public String getDatabaseGeneratedReferencedColumnValue() {
        return null;
    }
}