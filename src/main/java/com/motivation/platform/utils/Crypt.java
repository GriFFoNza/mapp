package com.motivation.platform.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Crypt {

    // Encryte Password with BCryptPasswordEncoder
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

}
