package com.motivation.platform.utils;

import com.motivation.platform.entity.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class FileUpload {

    public String gethash() {
        User user = AuthUtils.getCurrentUser();
        Date date = new Date();
        return DigestUtils.md5Hex(user.getUsername() + date.getTime()).toUpperCase();
    }

    public Map<String, String> loadFile(MultipartFile inputfile, String[] extensions, String new_name) {

        Map<String, String> fileinfo = new HashMap<>();
        if (Arrays.asList(extensions).contains(FilenameUtils.getExtension(inputfile.getOriginalFilename()))) {

            //String hashkey = gethash();
            String filename;
            if (new_name == null) {
                filename = inputfile.getOriginalFilename();
            } else {
                filename = new_name + "." + FilenameUtils.getExtension(inputfile.getOriginalFilename());
            }
            File currDir = new File("./upload/" + filename);
            String fileLocation = currDir.getAbsolutePath();
            //System.out.println(fileLocation);
            try {
                InputStream in = inputfile.getInputStream();
                FileOutputStream f = new FileOutputStream(fileLocation);
                int ch = 0;
                while ((ch = in.read()) != -1) {
                    f.write(ch);
                }
                f.flush();
                f.close();

                System.out.println("Загрузили файл:" + fileLocation);
                //fileinfo.put("hashKey",new_name);
                fileinfo.put("origFileName", inputfile.getOriginalFilename());
                fileinfo.put("fileName", filename);
                fileinfo.put("fileLocation", fileLocation);
                fileinfo.put("message", "Файл успешно загружен.");
            } catch (Exception e) {
                fileinfo.put("message", "Проблема при загрузке файла.");
                System.out.println("Проблема при загрузке файла. \n" + e);
            }
        } else {
            fileinfo.put("message", "Тип файла не подходит для загрузки.");
        }
        return fileinfo;
    }

}
