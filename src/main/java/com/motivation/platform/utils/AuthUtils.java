package com.motivation.platform.utils;

import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.User;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Set;

public class AuthUtils {

    public static boolean hasRole(User user, String roleName) {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(roleName));
    }

    public static User getCurrentUser() {
        //Returns User object of currently authenticated User
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof User) {
            return (User) principal;
        } else return /*null*/ new User();
    }

    public static void printUserCredentials(User user) {
        //Prints list of Roles and Units available for this user
        Set<Role> userRoles = user.getRoles();
        userRoles.forEach(e -> {
            System.out.println("USER:" + user.getUsername() + "ROLENAME:" + e.getFullname() + " UNIT:" + e.getUnit().getUnitname());
        });
    }
}
