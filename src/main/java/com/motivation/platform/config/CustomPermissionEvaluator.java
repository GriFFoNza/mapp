package com.motivation.platform.config;

import com.motivation.platform.entity.HasUnit;
import com.motivation.platform.entity.Role;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.service.GetEntityByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Collection;

@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    @Lazy
    @Autowired
    GetEntityByIdService getEntityService;

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {

        /* If object has Unit we use it for the further check
        Otherwise treating it like a Access Name
         */
        String accessType;
        if (!((targetDomainObject instanceof HasUnit) || (targetDomainObject instanceof String))) {
            System.out.println(this.getClass().getSimpleName().concat(" >> ") + " wrong object for access Type");
            return false;
        }
        Unit unit = null;
        if (targetDomainObject instanceof HasUnit) {
            unit = ((HasUnit) targetDomainObject).getUnit();
            accessType = targetDomainObject.getClass().getSimpleName();
        } else {
            accessType = targetDomainObject.toString();
        }
        String permissionStr = permission.toString();
        System.out.println(this.getClass().getSimpleName().concat(" >> ") + "TARGET DOMAIN OBJECT IS "
                + targetDomainObject);

        return this.checkPermission(authentication, accessType, permissionStr, unit);
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable serializable, String targetType,
                                 Object permission) {

        /* If Unit for objectType exists, we pass it for further verification */
        Unit unit = null;
        {
            Object object = null;
            try {
                Long objectId = (Long) serializable;
                object = getEntityService.getEntity(targetType, objectId);
            } catch (Exception e) {
                System.out.println(this.getClass().getSimpleName().concat(" >> ") + " can't get Unit by ID");
                return false;
            }
            if (!(object instanceof HasUnit)) {
                return false;
            }
            unit = ((HasUnit) object).getUnit();
        }
        String permissionStr = permission.toString();
        return this.checkPermission(authentication, targetType, permissionStr, unit);
    }

    @SuppressWarnings("unchecked")
    public boolean checkPermission(Authentication authentication, String accessType,
                                   String permission, Unit unit) {

        /* Compare object for access with user authorities */
        boolean grantPermission = false;
        if (authentication != null && accessType != null && permission != null) {
            Collection<Role> roles = (Collection<Role>) authentication.getAuthorities();
            for (Role role : roles) {
                /* Logic for ADMIN */
                if (role.getRolename().equalsIgnoreCase("ADMIN")) {
                    grantPermission = true;
                    break;
                }
                /* Logic for others */
                boolean checkRoleName = role.getRolename().equalsIgnoreCase(accessType);
                boolean checkPermission = role.getPermission().getName().equalsIgnoreCase(permission);
                boolean checkUnit = true;
                if (unit != null) {
                    checkUnit = role.getUnit().getId().equals(unit.getId());
                }
                grantPermission = checkRoleName & checkPermission & checkUnit;

                /* Outputting results of compare to console for debug */
                System.out.println(this.getClass().getSimpleName().concat(" >> ") + "comparing: " +
                        role.getRolename() + " vs " + accessType + " result: " + checkRoleName);
                System.out.println(this.getClass().getSimpleName().concat(" >> ") + "comparing: " +
                        role.getPermission().getName() + " vs " + permission + " result: " + checkPermission);
                if (unit != null) {
                    System.out.println(this.getClass().getSimpleName().concat(" >> ") + "comparing: " +
                            role.getUnit().getId() + " vs " + unit.getId() + " result: " + checkUnit);
                }

                if (grantPermission) {
                    break;
                }
            }
        }
        System.out.println(this.getClass().getSimpleName().concat(" >> ") + " permiss check total: "
                + grantPermission);

        return grantPermission;
    }

}