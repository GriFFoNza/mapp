package com.motivation.platform.config;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Overriding standart Spring's JdbcTokenRepositoryImpl to implement custom table instead of default
 */
public class JdbcTokenRepositoryImplCust extends JdbcTokenRepositoryImpl implements PersistentTokenRepository {
    // ~ Static fields/initializers
    // =====================================================================================

    public static final String DEF_TABLE_SQL = "MAPP.user_sessions";
    public static final String CREATE_TABLE_SQL = "create table " + DEF_TABLE_SQL + " (username varchar(64) not null, series varchar(64) primary key, "
            + "token varchar(64) not null, last_used timestamp not null)";
    public static final String DEF_TOKEN_BY_SERIES_SQL = "select username,series,token,last_used from " + DEF_TABLE_SQL + " where series = ?";
    public static final String DEF_INSERT_TOKEN_SQL = "insert into " + DEF_TABLE_SQL + "(username, series, token, last_used) values(?,?,?,?)";
    public static final String DEF_UPDATE_TOKEN_SQL = "update " + DEF_TABLE_SQL + " set token = ?, last_used = ? where series = ?";
    public static final String DEF_REMOVE_USER_TOKENS_SQL = "delete from " + DEF_TABLE_SQL + " where username = ?";

    // ~ Instance fields
    // ================================================================================================

    private final String tokensBySeriesSql = DEF_TOKEN_BY_SERIES_SQL;
    private final String insertTokenSql = DEF_INSERT_TOKEN_SQL;
    private final String updateTokenSql = DEF_UPDATE_TOKEN_SQL;
    private final String removeUserTokensSql = DEF_REMOVE_USER_TOKENS_SQL;
    private boolean createTableOnStartup;

    protected void initDao() {
        if (createTableOnStartup) {
            getJdbcTemplate().execute(CREATE_TABLE_SQL);
        }
    }

    public void createNewToken(PersistentRememberMeToken token) {
        getJdbcTemplate().update(insertTokenSql, token.getUsername(), token.getSeries(),
                token.getTokenValue(), token.getDate());
        System.out.println("Session store: create toked fetched");
    }

    public void updateToken(String series, String tokenValue, Date lastUsed) {
        getJdbcTemplate().update(updateTokenSql, tokenValue, lastUsed, series);
        System.out.println("Session store: update toked fetched");
    }

    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        System.out.println("Session store: get token fetched");
        try {
            return getJdbcTemplate().queryForObject(tokensBySeriesSql,
                    new RowMapper<PersistentRememberMeToken>() {
                        public PersistentRememberMeToken mapRow(ResultSet rs, int rowNum)
                                throws SQLException {
                            return new PersistentRememberMeToken(rs.getString(1), rs
                                    .getString(2), rs.getString(3), rs.getTimestamp(4));
                        }
                    }, seriesId);
        } catch (EmptyResultDataAccessException zeroResults) {
            if (logger.isDebugEnabled()) {
                logger.debug("Querying token for series '" + seriesId
                        + "' returned no results.", zeroResults);
            }
        } catch (IncorrectResultSizeDataAccessException moreThanOne) {
            logger.error("Querying token for series '" + seriesId
                    + "' returned more than one value. Series" + " should be unique");
        } catch (DataAccessException e) {
            logger.error("Failed to load token for series " + seriesId, e);
        }

        return null;
    }

    public void removeUserTokens(String username) {
        getJdbcTemplate().update(removeUserTokensSql, username);
        System.out.println("Session store: remove token fetched");
    }

    public void setCreateTableOnStartup(boolean createTableOnStartup) {
        this.createTableOnStartup = createTableOnStartup;
    }
}