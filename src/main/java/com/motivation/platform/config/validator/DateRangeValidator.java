package com.motivation.platform.config.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(
        validatedBy = {DateRangeValidatorImpl.class}
)
@Documented

public @interface DateRangeValidator {

    String message() default "дата окончания должна быть после даты начала";

    String DateStart();

    String DateEnd();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
