package com.motivation.platform.config.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(
        validatedBy = {LastDateOfMonthValidatorImpl.class}
)
@Documented

public @interface LastDateOfMonthValidator {

    String message() default "дата должна быть последним днём месяца";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
