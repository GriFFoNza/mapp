package com.motivation.platform.config.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Method;
import java.sql.Date;


public class DateRangeValidatorImpl implements ConstraintValidator<DateRangeValidator, Object> {
    private String DateStart;
    private String DateEnd;
    private boolean valid;

    public void initialize(DateRangeValidator validateDateRange) {
        DateStart = validateDateRange.DateStart();
        DateEnd = validateDateRange.DateEnd();
    }

    @SuppressWarnings("unchecked")
    public boolean isValid(Object object,
                           ConstraintValidatorContext constraintValidatorContext) {
        try {
            Class clazz = object.getClass();

            Date startDate = null;
            Method startGetter = clazz.getMethod(getAccessorMethodName(DateStart));
            Object startGetterResult = startGetter.invoke(object);
            if (startGetterResult instanceof Date) {
                startDate = (Date) startGetterResult;
            } else {
                valid = false;
            }

            Date endDate = null;
            Method endGetter = clazz.getMethod(getAccessorMethodName(DateEnd));
            Object endGetterResult = endGetter.invoke(object);
            if (endGetterResult instanceof Date) {
                endDate = (Date) endGetterResult;
            } else {
                valid = true;
            }

            if (endDate != null && startDate != null) {
                valid = (startDate.before(endDate));
            }
        } catch (Throwable e) {
            valid = false;
            e.printStackTrace();
        }
        if (!valid) {
            constraintValidatorContext.buildConstraintViolationWithTemplate(constraintValidatorContext.getDefaultConstraintMessageTemplate())
                    .addPropertyNode(DateEnd).addConstraintViolation().disableDefaultConstraintViolation();
        }
        return valid;
    }

    private String getAccessorMethodName(String property) {
        return "get" + Character.toUpperCase(property.charAt(0)) +
                property.substring(1);
    }
}
