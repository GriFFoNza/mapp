package com.motivation.platform.config.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.sql.Date;
import java.util.Calendar;

public class LastDateOfMonthValidatorImpl implements ConstraintValidator<LastDateOfMonthValidator, Date> {
    @Override
    public void initialize(LastDateOfMonthValidator constraintAnnotation) {

    }

    @Override
    public boolean isValid(Date date, ConstraintValidatorContext constraintValidatorContext) {

        Calendar DayOfMonth = Calendar.getInstance();
        DayOfMonth.setTime(date);
        Calendar LastDayOfMonth = Calendar.getInstance();
        LastDayOfMonth.setTime(date);
        LastDayOfMonth.set(Calendar.DAY_OF_MONTH, LastDayOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
        return LastDayOfMonth.getTime().equals(DayOfMonth.getTime());
    }
}
