package com.motivation.platform.config.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/* For custom input validation in future */
public class StringValidatorImpl implements ConstraintValidator<StringValidator, Object> {

    @Override
    public boolean isValid(Object objectCheck, ConstraintValidatorContext ctx) {
        if (!(objectCheck instanceof String)) {
            return false;
        }
        String stringCheck = (String) objectCheck;
        boolean result = !stringCheck.matches("[0-9]+");
        System.out.println(this.getClass().getSimpleName().concat(" >> ") + "Result check is: " + result);
        return result;
    }

}


