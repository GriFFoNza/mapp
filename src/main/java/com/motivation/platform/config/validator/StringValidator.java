package com.motivation.platform.config.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = StringValidatorImpl.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD, /*new*/ ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})

/* For custom input validation in future */
public @interface StringValidator {
    String message() default "Только цифры вводить нельзя";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
