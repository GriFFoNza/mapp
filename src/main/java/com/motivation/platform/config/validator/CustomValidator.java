package com.motivation.platform.config.validator;

import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.HibernateValidatorConfiguration;
import org.hibernate.validator.cfg.ConstraintMapping;
import org.hibernate.validator.cfg.GenericConstraintDef;
import org.springframework.stereotype.Component;

import javax.validation.Validation;
import javax.validation.Validator;

@Component

public class CustomValidator {

    /* Validates variables in code inline. Usage:
    Validator validator = new GetCustomValidator().getValidator(Object.class (optional), ValidatorInterface.class);
    validator.validate(testStr).forEach(e -> System.out.println(e.getMessage()));
     */

    public Validator getValidator(Class restrictionClass) {
        return this.getValidator(null, restrictionClass);
    }

    @SuppressWarnings("unchecked")
    public Validator getValidator(Class objectInput, Class restrictionClass) {
        Class cls = Object.class;
        if (objectInput != null) {
            cls = objectInput;
        }

        HibernateValidatorConfiguration configuration = Validation
                .byProvider(HibernateValidator.class)
                .configure();

        ConstraintMapping constraintMapping = configuration.createConstraintMapping();

        constraintMapping
                .type(cls)
                .constraint(new GenericConstraintDef<>(restrictionClass))
        ;

        Validator validator = configuration.addMapping(constraintMapping)
                .buildValidatorFactory()
                .getValidator();

        return validator;
    }

}

        /*
        Depreciated

        constraintMapping
                .constraintDefinition(StringValidator.class)
                .includeExistingValidators(true)
                .validatedBy(StringValidatorImpl.class)
                .validateType(String.class)
                ;
        */