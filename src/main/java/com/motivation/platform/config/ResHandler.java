package com.motivation.platform.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ResHandler implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //Пробрасываем ресурсы
        registry.addResourceHandler("/resource/jquery/**")//
                .addResourceLocations("classpath:/META-INF/resources/webjars/jquery/3.0.0/");
        registry.addResourceHandler("/resource/popper/**")//
                .addResourceLocations("classpath:/META-INF/resources/webjars/popper.js/1.14.3/umd/");
        registry.addResourceHandler("/resource/bootstrap/**")//
                .addResourceLocations("classpath:/META-INF/resources/webjars/bootstrap/4.4.1/");
    }
}