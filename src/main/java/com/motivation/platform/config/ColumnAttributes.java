package com.motivation.platform.config;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class ColumnAttributes {
    @Getter private String field;
    @Getter private Boolean isEditable;
    @Getter @Setter private String width;
    @Getter @Setter private Integer widthPixel;
    @Getter @Setter private String caption;
    @Getter @Setter private Boolean hidden;
    public ColumnAttributes(String fieldName, Boolean isEditable) {
        this.field = fieldName;
        this.isEditable = isEditable;
        this.caption = fieldName;
        this.hidden = false;
        this.width = null;
        this.widthPixel = null;
    }


}
