package com.motivation.platform.config;

import com.motivation.platform.excel.ExcelPOIHelper;
import com.motivation.platform.service.UserDetailsServiceImpl;
import com.motivation.platform.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity //Turns on Spring's web security component
//@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserService userService;
    @Autowired
    UserDetailsServiceImpl userDetailsService;
    @Autowired
    DataSource dataSource;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        //Password encoder settings to avoid storing plain passwords in DB
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        //Choose where to save user session: DB or memory (in our case: DB)
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImplCust();
        db.setDataSource(dataSource);
        return db;
    }

    @Bean
    public ExcelPOIHelper excelPOIHelper() {
        return new ExcelPOIHelper();
    }

    @Bean
    public HttpTraceRepository htttpTraceRepository() {
        return new InMemoryHttpTraceRepository();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        //Settings service to find User in DB + password
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    /* Role hierarchy*/
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //Main method for Spring's web security
        http
                // URL checks
                .authorizeRequests()
                //.expressionHandler(webExpressionHandler()) //Added for using Role hierarchy
                //.antMatchers("/xxx").permitAll() //All users can see it
                .antMatchers("/resource/**").permitAll() //All users can see it
                .antMatchers("/users/**", "/roles/**").hasAuthority("ADMIN") //Only users with Admin Role can see it
                //.anyRequest().permitAll() //Disable authorization for all resources
                .anyRequest().authenticated() //All another URL requires authorization
                .and()
                // Login forms
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/main")//Redirect page if
                .failureUrl("/login?error")
                .usernameParameter("username") //parse from POST
                .passwordParameter("password") //parse from POST
                .permitAll()
                .and()
                // Actions when logoff
                .logout()
                .permitAll() //разлогинится могут все
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .and()
                // Exceptions
                .exceptionHandling()
                .accessDeniedPage("/denied")
                .and()
                //Cookies
                .rememberMe()
                .tokenRepository(this.persistentTokenRepository()) //Choose sessions repository
                .rememberMeParameter("remember-me")
                //.alwaysRemember(true) //- always remember ignoring checkbox
                .tokenValiditySeconds(1 * 24 * 60 * 60) //24H
                .and()
                .csrf().disable()
        ;
    }
}
