package com.motivation.platform.controller;

import com.motivation.platform.entity.Goal;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.service.GoalService;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Date;

@Controller
@SessionAttributes("goal")
public class GoalControllerBKP {

    @Autowired
    private GoalService goalService;
    @Autowired
    private UnitService unitService;

    public static final String WEB_URL = "/goals"; //url mapping
    public static final String WEB_VIEW_URL = "goals"; //view src
    public static final String WEB_FORM_URL = "goals_form"; //add-edit form src
    public static final String AUTHORITY_NAME = "GOAL"; //access name

    //View all records
    @GetMapping(WEB_URL)
    @PreAuthorize("hasAuthority('"+AUTHORITY_NAME+"')")
    public String goalList (Model model, Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<Goal> goals;
        goals = goalService.getByRoles(currentUser.getRoles()); //new
        /*
        if (AuthUtils.hasRole(currentUser, "ADMIN")) {
            goals = goalService.getAll();
        } else {
            goals = goalService.getByRoles(currentUser.getRoles()); //new
        }*/
        //System.out.println("INJECTION TEST" + currentUser.testInjection);
        model.addAttribute("goals", goals);
        return WEB_VIEW_URL;
    }

    //Post record
    @PostMapping(WEB_URL)
    @PreAuthorize("hasPermission(#goal,'WRITE')")
    public String save (@ModelAttribute("goal") Goal goal,  RedirectAttributes redirect, SessionStatus sessionStatus) {
        Date date = new Date();
        User currentUser = AuthUtils.getCurrentUser();
        if ((goal.getDate_add()==null) || (goal.getUser_add()==null)) {
            goal.setDate_add(date);
            goal.setUser_add(currentUser);
        }
        goal.setDate_upd(date);
        goal.setUser_upd(currentUser);
        goalService.save(goal);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }

    //Add or edit record form
    @GetMapping({WEB_URL + "/edit/{id}", WEB_URL + "/new"})
    @PreAuthorize("hasPermission('"+AUTHORITY_NAME+"','WRITE')")
    public String edit (@PathVariable (required = false) Long id, Model model, HttpServletRequest http) {
        User currentUser = AuthUtils.getCurrentUser();
        Goal goal = new Goal();
        Iterable<Unit> units;
        if (!http.getServletPath().equals(WEB_URL + "/new") ) {
            goal = goalService.getById(id);
        }
        if (AuthUtils.hasRole(currentUser, "ADMIN")) {
            units = unitService.getAll();
        } else {
            units = unitService.getUnitsByRole(currentUser.getRoles());
        }
        model.addAttribute("goal", goal);
        model.addAttribute("units", units);
        return WEB_FORM_URL;
    }

    //Delete record
    @GetMapping(WEB_URL + "/delete/{id}")
    @PreAuthorize("hasPermission(#id, 'GOAL','WRITE')")
    public String delete (@PathVariable Long id, RedirectAttributes redirect) {
        goalService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }

}

