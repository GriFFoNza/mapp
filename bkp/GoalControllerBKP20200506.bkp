package com.motivation.platform.controller;

import com.motivation.platform.entity.Goal;
import com.motivation.platform.entity.Unit;
import com.motivation.platform.entity.User;
import com.motivation.platform.service.GoalService;
import com.motivation.platform.service.UnitService;
import com.motivation.platform.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;

@Controller
@RequestMapping("/goals")
@SessionAttributes("goal")
public class GoalControllerBKP20200506 {

    @Autowired
    private GoalService goalService;
    @Autowired
    private UnitService unitService;

    public static final String WEB_URL = "/goals"; //url mapping
    public static final String WEB_VIEW_TEMPLATE = "goals"; //view src
    public static final String WEB_FORM_TEMPLATE = "goals_form"; //add-edit form src
    public static final String AUTHORITY_NAME = "GOAL"; //access name

    //View all records
    @GetMapping()
    @PreAuthorize("hasAuthority('"+AUTHORITY_NAME+"')")
    public String goalList (Model model, Principal prn) {
        User currentUser = AuthUtils.getCurrentUser();
        Iterable<Goal> goals;
        goals = goalService.getByRoles(currentUser.getRolesInherited()); //new
        model.addAttribute("goals", goals);
        return WEB_VIEW_TEMPLATE;
    }

    //Post record
    @PostMapping()
    @PreAuthorize("hasPermission(#goal,'WRITE')")
    public String save (@Valid @ModelAttribute("goal")Goal goal
                     ,BindingResult bindingResult
                     ,RedirectAttributes redirect
                     ,SessionStatus sessionStatus) {

        if (bindingResult.hasErrors()) {
            return WEB_FORM_TEMPLATE;
        }

        Date date = new Date();
        User currentUser = AuthUtils.getCurrentUser();
        if ((goal.getDate_add()==null) || (goal.getUser_add()==null)) {
            goal.setDate_add(date);
            goal.setUser_add(currentUser);
        }
        goal.setDate_upd(date);
        goal.setUser_upd(currentUser);
        goalService.save(goal);
        redirect.addFlashAttribute("systemMessage", "Запись сохранена");
        sessionStatus.isComplete(); //Clear session attributes
        return "redirect:" + WEB_URL;
    }

    //Add or edit record form
    @GetMapping({"/edit/{id}", "/new"})
    @PreAuthorize("hasPermission('"+AUTHORITY_NAME+"','WRITE')")
    public String edit (@PathVariable (required = false) Long id, Model model, HttpServletRequest http) {
        User currentUser = AuthUtils.getCurrentUser();
        Goal goal = new Goal();
         Iterable<Unit> units = unitService.getUnitsByRole(currentUser.getRolesInherited());
        if (!http.getServletPath().contains("/new") ) {
            goal = goalService.getById(id);
        }
        model.addAttribute("goal", goal);
        model.addAttribute("units", units);
        return WEB_FORM_TEMPLATE;
    }

    //Delete record
    @GetMapping("/delete/{id}")
    @PreAuthorize("hasPermission(#id, 'GOAL','WRITE')")
    public String delete (@PathVariable Long id, RedirectAttributes redirect) {
        goalService.deleteById(id);
        redirect.addFlashAttribute("systemMessage", "Запись удалена");
        return "redirect:" + WEB_URL;
    }

}

